CREATE DATABASE authtest;
USE authtest;

CREATE  TABLE users2 (
  email VARCHAR(45) NOT NULL ,
  password VARCHAR(45) NOT NULL ,
  enabled TINYINT NOT NULL DEFAULT 1 ,
  PRIMARY KEY (email));
