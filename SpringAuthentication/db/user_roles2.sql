USE authtest;

CREATE TABLE user_roles2 (
  user_role_id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_email_role (role,email),
  KEY fk_email_idx (email),
  CONSTRAINT fk_email FOREIGN KEY (email) REFERENCES users2 (email));
