package com.gn.spring.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

	/**
	 * This will automatically register the springSecurityFilterChain Filter for
	 * every URL in your application. If Filters are added within other
	 * WebApplicationInitializer instances we can use -@Order to control the
	 * ordering of the Filter instances. Without this the Authentication DOES NOT work.
	 */
}
