package com.gn.spring.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.gn.spring.Constants;

// any of them can be used
//@EnableGlobalAuthentication
//@EnableGlobalMethodSecurity

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		System.err.println("++++ SecurityConfig.configureGlobal");
		if(false) {
		 auth.inMemoryAuthentication().withUser("user").password("1234").roles("USER");
//		 auth.jdbcAuthentication().dataSource(dataSource).withDefaultSchema().withUser("user").password("1234")
//		 .roles("USER");
//		auth.jdbcAuthentication().dataSource(dataSource)
//				.usersByUsernameQuery("select username,password, enabled from users where username=?")
//				.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
		} else {
			
			if (Constants.USE_EMAIL_AUTH) {
				auth.jdbcAuthentication().dataSource(dataSource)
				.usersByUsernameQuery("select email,password, enabled from users2 where email=?")
				.authoritiesByUsernameQuery("select email, role from user_roles2 where email=?");
			} else {
				auth.jdbcAuthentication().dataSource(dataSource)
						.usersByUsernameQuery("select username,password, enabled from users where username=?")
						.authoritiesByUsernameQuery("select username, role from user_roles where username=?");
			}
		}
	}

//	http.authorizeRequests().antMatchers("/", "/login").permitAll()
	@Override
	public void configure(HttpSecurity http) throws Exception {
		System.err.println("++++ SecurityConfig.configure");
		
//		http.authorizeRequests().antMatchers("/", "/hello").permitAll()
		
		/*
		 * Those pages which are not mentioned can be accessed without login.
		 */
		http.authorizeRequests()
		.antMatchers("/user/**", "/hello").access("hasRole('ROLE_USER') or hasRole('USER') or hasRole('ROLE_ADMIN')")
			.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
			.and()
			  .formLogin().loginPage("/login").failureUrl("/login?error")
			  .usernameParameter("username").passwordParameter("password")
			.and()
			  .logout().logoutSuccessUrl("/login?logout")
			.and()
			  .exceptionHandling().accessDeniedPage("/403")
			.and()
			  .csrf();
	}
}
