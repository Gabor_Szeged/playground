package com.gn.spring;

public class Constants {
	
	/**
	 * True -> uses the email+password, False -> userName+password
	 */
	public static boolean USE_EMAIL_AUTH = true;

}
