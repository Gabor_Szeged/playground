/*CREATE DATABASE wordcards;*/
USE wordcards;

CREATE TABLE USERS (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	EMAIL VARCHAR(50) NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	PASSWORD VARCHAR(50) NOT NULL,
	DATE_OF_REGISTRATION DATE NOT NULL,
	ACTIVE BOOLEAN DEFAULT false NOT NULL,
	PRIMARY KEY (ID),
	UNIQUE (EMAIL)
);

CREATE TABLE USER_ROLES (
  USER_ROLE_ID INTEGER NOT NULL AUTO_INCREMENT,
  EMAIL varchar(50) NOT NULL,
  ROLE varchar(30) NOT NULL,
  PRIMARY KEY (USER_ROLE_ID),
  UNIQUE KEY UNI_EMAIL_ROLE (EMAIL,ROLE),
  KEY FK_USER_ID_IDX (EMAIL),
  CONSTRAINT FK_EMAIL FOREIGN KEY (EMAIL) REFERENCES USERS (EMAIL)
);

CREATE TABLE FORGOT_PASSWORD (
	USER_ID INTEGER NOT NULL,
	FOREIGN KEY (USER_ID) REFERENCES USERS(ID)
);

CREATE TABLE STORIES (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	USER_ID INTEGER NOT NULL,
	STORY VARCHAR(50) NOT NULL,
	PRIMARY KEY (ID),
	FOREIGN KEY(USER_ID) REFERENCES USERS(ID)
);

CREATE TABLE WORDCARDS (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	USER_ID INTEGER REFERENCES USERS(ID),
	STORY_ID INTEGER REFERENCES STORIES(ID),
	WORD VARCHAR(40) NOT NULL,
	PRONUNCIATION VARCHAR(50),
	DEFINITION VARCHAR(250) NOT NULL,
	USAGES VARCHAR(500) NOT NULL,
	PACK SMALLINT NOT NULL,
	TIME_MOVED BIGINT NOT NULL,
	LANGUAGE_ID SMALLINT DEFAULT 1 NOT NULL,
	DIFFICULT BOOLEAN DEFAULT false NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE IDIOMCARDS (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	USER_ID INTEGER REFERENCES USERS(ID),
	KEY_WORD VARCHAR(40) NOT NULL,
	PRONUNCIATION VARCHAR(50),
	IDIOM VARCHAR(100) NOT NULL,
	USAGES VARCHAR(500) NOT NULL,
	PACK SMALLINT NOT NULL,
	TIME_MOVED DATE NOT NULL,
	LANGUAGE_ID SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
);