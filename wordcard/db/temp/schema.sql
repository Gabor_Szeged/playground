/*CREATE DATABASE wordcards;*/
USE wordcards;
 
CREATE TABLE USERS (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	EMAIL VARCHAR(50) NOT NULL,
	NAME VARCHAR(50) NOT NULL,
	PASSWORD VARCHAR(50) NOT NULL,
	DATE_OF_REGISTRATION DATE NOT NULL,
	ACTIVE BOOLEAN DEFAULT false NOT NULL,
	PRIMARY KEY (ID),
	UNIQUE (EMAIL)
);

CREATE TABLE USER_ROLES (
  USER_ROLE_ID INTEGER NOT NULL AUTO_INCREMENT,
  EMAIL varchar(45) NOT NULL,
  ROLE varchar(45) NOT NULL,
  PRIMARY KEY (USER_ROLE_ID),
  UNIQUE KEY UNI_EMAIL_ROLE (EMAIL,ROLE),
  KEY FK_USER_ID_IDX (EMAIL),
  CONSTRAINT FK_EMAIL FOREIGN KEY (EMAIL) REFERENCES USERS (EMAIL)
);
	
CREATE TABLE WORDCARDS (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	WORD VARCHAR(40) NOT NULL,
	PHONETICS VARCHAR(25),
	PACK SMALLINT NOT NULL,
	TIME_MOVED DATE NOT NULL,
	USER_ID INTEGER REFERENCES USER(ID),
	LANGUAGE_ID SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE WORD_DEFINITIONS_AND_USAGES (
	ID_WORD INTEGER NOT NULL,
	ID INTEGER NOT NULL,
	DEFINITION VARCHAR(250) NOT NULL,
	USAGES VARCHAR(500) NOT NULL,
	FOREIGN KEY (ID_WORD) REFERENCES WORDCARDS(ID),
	PRIMARY KEY (ID, ID_WORD)
);
	 
CREATE TABLE IDIOMCARDS (
	ID INTEGER NOT NULL AUTO_INCREMENT,
	IDIOM VARCHAR(100) NOT NULL,
	KEY_WORD VARCHAR(40) NOT NULL,
	PHONETITCS VARCHAR(25),
	USAGES VARCHAR(500) NOT NULL,
	PACK SMALLINT NOT NULL,
	TIME_MOVED DATE NOT NULL,
	USER_ID INTEGER REFERENCES USER(ID),
	LANGUAGE_ID SMALLINT DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
);