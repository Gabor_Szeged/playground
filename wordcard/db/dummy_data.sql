USE wordcards;

INSERT INTO USERS(email,name,password,date_of_registration,active)
VALUES ('user1@email.com', 'user1', 'e10adc3949ba59abbe56e057f20f883e', '2016-04-01', true);
INSERT INTO USERS(email,name,password,date_of_registration,active)
VALUES ('user2@email.com', 'user2', 'e10adc3949ba59abbe56e057f20f883e', '2016-04-01', true);
INSERT INTO USERS(email,name,password,date_of_registration,active)
VALUES ('gabor@email.com', 'Gabor Nagy', 'e10adc3949ba59abbe56e057f20f883e', '2016-04-01', true);

INSERT INTO USER_ROLES (email, role)
VALUES ('user1@email.com', 'ROLE_USER');
INSERT INTO USER_ROLES (email, role)
VALUES ('user1@email.com', 'ROLE_ADMIN');
INSERT INTO USER_ROLES (email, role)
VALUES ('user2@email.com', 'ROLE_USER');
INSERT INTO USER_ROLES (email, role)
VALUES ('gabor2@email.com', 'ROLE_USER');

INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'table', 'ʒθðæ', 'a flat surface, usually supported by four legs, used for putting things on', 'There was a really noisy table behind us celebrating someone s birthday.', 1, 1463677239017, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'dog', 'dog', 'a common animal with four legs, especially kept by people as a pet or to hunt or guard things', 'We could hear dogs barking in the distance.', 1, 1463677239088, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'window', 'window', 'a space usually filled with glass in the wall of a building or in a vehicle, to allow light and air in and to allow people inside the building to see out', 'He caught me staring out of the window.', 2, 1463677239088, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'flower', 'flower', 'the part of a plant that is often brightly coloured and has a pleasant smell, or the type of plant that produces these', 'These flowers are brightly coloured in order to attract butterflies.', 1, 1463677239188, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'elephant', 'elephant', 'a very large grey mammal that has a trunk (= long nose) with which it can pick things up', 'In the past eight years, the elephant population in Africa has been halved.', 1, 1463677239288, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'heir', 'heir', 'the person who has the legal right to receive the property or title of another person', 'John was the sole heir to a vast estate.', 1, 1463677239288, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'tomboy', 'tomboy', 'a girl who likes playing the same games as boys', 'In tomboy, a French movie released last fall, the main character is an androgynous girl who purposely passes as a boy.', 1, 1463677239288, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'skin graft', 'skin graft', 'a medical operation in which healthy skin is removed from one part of the body and used on another part to replace burned or damaged skin', 'The most difficult part, however, for Ryan, was getting used to the skin graft that doctors covered his wound with.', 1, 1463677239288, 1);
INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, 'facial', 'facial', 'on your face or relating to your face', 'Vitor s facial expression didnt change. Facially the boys are similar', 1, 1463677239288, 1);

INSERT INTO WORDCARDS (USER_ID, WORD, PRONUNCIATION, DEFINITION, USAGES, PACK, TIME_MOVED, LANGUAGE_ID)
VALUES (1, '', '', '', '', 1, 1463677239288, 1);

