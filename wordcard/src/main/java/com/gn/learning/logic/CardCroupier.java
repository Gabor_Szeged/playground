package com.gn.learning.logic;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.gn.learning.model.WordCard;

public class CardCroupier {

	private static Object mutexSort = new Object();

	// TODO when to remove the pack for the user? After logout? At the moment at
	// logout with deletePack(userID).
	private static Map<Integer, List<WordCard>> userId2Pack = new ConcurrentHashMap<>();

	public static void deletePack(int userID) {
		userId2Pack.remove(userID);
	}

	public static void storePack(List<WordCard> pack, int userId) {
		sort(pack);
		userId2Pack.put(userId, pack);
	}

	public static WordCard getNextCard(int userId) {
		List<WordCard> pack = userId2Pack.get(userId);
		if (pack.size() > 0) {
			if (pack.size() > 1) {
				sort(pack);
			}
			return pack.get(0);
		}
		return null;
	}

	private static void sort(List<WordCard> pack) {
		synchronized (mutexSort) {
			Collections.sort(pack, new Comparator<WordCard>() {
				@Override
				public int compare(WordCard wc1, WordCard wc2) {
					if (wc1.getTimeMoved() > wc2.getTimeMoved()) {
						return 1;
					}
					if (wc1.getTimeMoved() == wc2.getTimeMoved()) {
						return 0;
					}
					return -1;
				}
			});
		}
	}

	public static int getNextPackNumber(int pack) {
		if (pack < 5) {
			return ++pack;
		}
		return pack;
	}

	public static WordCard getCard(int cardId, int userId) {
		List<WordCard> pack = userId2Pack.get(userId);
		for (WordCard wordCard : pack) {
			if (wordCard.getId() == cardId) {
				return wordCard;
			}
		}
		return null;
	}

	public static void removeCard(int userId, WordCard wordCard) {
		List<WordCard> pack = userId2Pack.get(userId);
		pack.remove(wordCard);
	}

	public static int getCardPackSize(int userId) {
		List<WordCard> pack = userId2Pack.get(userId);
		return pack.size();
	}

	public static long getTimeMovedBack(int userId, int backPosition) {
		List<WordCard> pack = userId2Pack.get(userId);
		if (pack.size() > backPosition && backPosition > 2) {
			long time1 = pack.get(backPosition).getTimeMoved();
			long time2 = pack.get(backPosition - 1).getTimeMoved();
			int diff =  (int) (Math.abs(time1 - time2) / 2);
			return time2 + diff;
		}
		return System.currentTimeMillis();
	}
}
