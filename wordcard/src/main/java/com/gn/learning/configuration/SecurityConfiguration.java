package com.gn.learning.configuration;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.GenericFilterBean;

import com.gn.learning.encrypt.PasswordEncrypterFactory;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery("SELECT EMAIL,PASSWORD, ACTIVE FROM USERS WHERE EMAIL=?")
				.authoritiesByUsernameQuery("SELECT EMAIL, ROLE FROM USER_ROLES WHERE EMAIL=?");
	}

	/*
	 * If your files are not in standard dirs, you can add the following
	 * configuration:
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/lib/**"); // like this
	}

	/**
	 * When a user wants to enter a page and that page under authorization (e.g.
	 * /main) then it will show the /login page and if the login was successful
	 * the originally wanted page "/main" will be show if the user has the right
	 * to view it.
	 */

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.addFilterBefore(getPasswordFilter(), UsernamePasswordAuthenticationFilter.class);
		http.authorizeRequests().antMatchers("/resources/img/**").permitAll()
			.antMatchers("/resources/lib/**").permitAll()
			.antMatchers("/resources/css/**").permitAll()
			.antMatchers("/").permitAll() // this
												// is
												// also
												// the
												// login
												// page
				.antMatchers("/registration").permitAll()
				.antMatchers("/forgot-password").permitAll()
				.antMatchers("/password-reset").permitAll()
				.antMatchers("/test").permitAll()
				.antMatchers("/getSearchResult").permitAll()
				.anyRequest().authenticated().and()
				.formLogin().loginPage("/login").usernameParameter("email").passwordParameter("password")
				.permitAll().defaultSuccessUrl("/main").and()
				.logout().permitAll().and()
				.csrf().and()
				.exceptionHandling()
				.accessDeniedPage("/Access_Denied");
	}

	private Filter getPasswordFilter() {

		Filter filter = new GenericFilterBean() {

			@Override
			public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
				if (!(request instanceof HttpServletRequestPasswordEncrypter)) {
					request = new HttpServletRequestPasswordEncrypter((HttpServletRequest) request);
				}
				chain.doFilter(request, response);
			}
		};

		return filter;
	}

	class HttpServletRequestPasswordEncrypter extends HttpServletRequestWrapper {

		public HttpServletRequestPasswordEncrypter(HttpServletRequest request) {
			super(request);

		}

		@Override
		public String getParameter(String name) {
			String parameter = super.getParameter(name);
			if (isPassowrdParameter(name)) {
				parameter = PasswordEncrypterFactory.getEncrypter(PasswordEncrypterFactory.Algorithm.MD5).encrypt(parameter);
			}
			return parameter;
		}

		/**
		 * <pre>
		 * The "password" is used by the login.ftl and registration.ftl pages. 
		 * The "password2" is used by the registraion.ftl page
		 * </pre>
		 */
		private boolean isPassowrdParameter(String name) {
			return UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY.equals(name) || "password2".equals(name);
		}

	}
}
