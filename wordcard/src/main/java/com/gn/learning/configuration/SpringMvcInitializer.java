package com.gn.learning.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.gn.learning.web.WordCreatorController;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordCreatorController.class);

	@Override
	protected Class<?>[] getRootConfigClasses() {
		LOGGER.info("+++ SpringMvcInitializer.gerRootConfigClasses()");
		return new Class[] { RootConfiguration.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		LOGGER.info("+++ SpringMvcInitializser.getSErletConfigClasses()");
		return new Class[] { WebConfiguration.class, DataSourceConfiguration.class, SecurityConfiguration.class,
				BeansConfiguration.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
