package com.gn.learning.configuration;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = { "com.gn.learning.*" })
public class DataSourceConfiguration {

	private static Logger LOGGER = LoggerFactory.getLogger(DataSourceConfiguration.class);

	@Autowired
	private Environment env;

	@Autowired
	DataSource dataSource;

	@Bean
	public DriverManagerDataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		String driverName = env.getProperty("db_driver");
		LOGGER.info("Database driver name: {}", driverName);
		dataSource.setDriverClassName(driverName);

		// dataSource.setUrl("jdbc:mysql://localhost:3306/wordcards");

		// Fri Apr 01 10:24:39 CEST 2016 WARN: Establishing SSL connection
		// without server's identity verification is not recommended. According
		// to MySQL 5.5.45+, 5.6.26+ and 5.7.6+ requirements SSL connection must
		// be established by default if explicit option isn't set. For
		// compliance with existing applications not using SSL the
		// verifyServerCertificate property is set to 'false'. You need either
		// to explicitly disable SSL by setting useSSL=false, or set useSSL=true
		// and provide truststore for server certificate verification.
		// dataSource.setUrl("jdbc:mysql://localhost:3306/wordcards?autoReconnect=true&useSSL=false&useUnicode=yes&characterEncoding=UTF-8");

		String url = env.getProperty("db_url");
		LOGGER.info("Database url: {}", url);
		dataSource.setUrl(url);

		String userName = env.getProperty("db_userName");
		LOGGER.info("Database user name: {}", userName);
		dataSource.setUsername(userName);

		String password = env.getProperty("db_psw");
		LOGGER.info("Database driver password: {}", password);
		dataSource.setPassword(password);

		return dataSource;
	}

	@Bean
	public SqlSessionFactoryBean sqlSessionFactory() {
		SqlSessionFactoryBean sqlSessinFactorBean = new SqlSessionFactoryBean();
		sqlSessinFactorBean.setDataSource(dataSource);
		sqlSessinFactorBean.setConfigLocation(new ClassPathResource("com/gn/learning/configuration/config.xml"));
		return sqlSessinFactorBean;
	}

}