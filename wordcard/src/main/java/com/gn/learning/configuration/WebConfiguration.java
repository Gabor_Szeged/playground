package com.gn.learning.configuration;

import java.util.Hashtable;
import java.util.Map;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.gn.learning")
public class WebConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	private ServletContext context;

	@Bean
	public FreeMarkerConfigurer freemarkerConfig() {
		System.out.println("context: " + context);
		FreeMarkerConfigurer config = new FreeMarkerConfigurer() {

//			@Override
//			protected void postProcessConfiguration(freemarker.template.Configuration config) throws IOException, TemplateException {
//				// The first 2 loaders allow to load .ftl templates in war files
//				// from "/WEB-INF/ftl" and from regular jar files from
//				// src/resources/templates.
//				WebappTemplateLoader WebAppTplLoader = new WebappTemplateLoader(context, "/WEB-INF/ftl");
//				if (context instanceof ClassTemplateLoader) {
//					ClassTemplateLoader classTplLoader = new ClassTemplateLoader(((ClassTemplateLoader) context).getClassLoader(), "/templates");
//					// And the baseMvcTplLoader loader to get the spring.ftl from
//					// org.springframework.web.servlet.view.freemarker.
//					ClassTemplateLoader baseMvcTplLoader = new ClassTemplateLoader(FreeMarkerConfigurer.class, "");
//					MultiTemplateLoader mtl = new MultiTemplateLoader(new TemplateLoader[] { WebAppTplLoader, classTplLoader, baseMvcTplLoader });
//					config.setTemplateLoader(mtl);
//				}
//
//			}
		};
		config.setTemplateLoaderPath("/WEB-INF/ftl/");
//		config.setPreferFileSystemAccess(true);


		// config.setDefaultEncoding("utf-8");
		// Locale locale = config.getConfiguration().getLocale();
		// for desktop application
		// config.getConfiguration().setLocale(Locale.GERMAN);

		if (false) {
			Map<String, Object> variables = new Hashtable<>();
			variables.put("xml_escape", "freemarker.template.utility.XmlExcaée");
			config.setFreemarkerVariables(variables);
		}

		return config;
	}
	
	

	@Bean
	public ViewResolver viewResolver() {
		FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
		// viewResolver.setViewClass(FreeMarkerView.class);
		viewResolver.setCache(true);
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".ftl");
		// this was needed to be able to use the special characters for
		// phonetics
		viewResolver.setContentType("text/html;charset=UTF-8");
		// If you want to use security tags in freemarker the escense are this
		// two lines:
		viewResolver.setExposeSpringMacroHelpers(true);
		viewResolver.setExposeRequestAttributes(true);
		return viewResolver;
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
		super.configureDefaultServletHandling(configurer);
	}

	// private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
	// "classpath:/META-INF/resources/",
	// "classpath:/resources/", "classpath:/static/", "classpath:/public/",
	// "WEB-INF/resources/", "WEB-INF/resources/lib/","WEB-INF/resources/img/"
	// };

	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "WEB-INF/resources/" };

	/*
	 * Configure ResourceHandlers to serve static resources like CSS/ Javascript
	 * etc...
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("resources/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
	}

	/**
	 * This mapping is stronger than this in the
	 * controller @RequestMapping(value = "/login", method = RequestMethod.GET)
	 * public String loginPage(). Maybe this setting can be used dynamically?
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// registry.addViewController("login").setViewName("home");
		// registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}

	/**
	 * <pre>
	 *  ResourceBundleMessageSource: It is used to handle message source by specifying property file base name.
	 *  It uses java.util.ResourceBundle that caches the bundle forever which is a limitation.
	 *
	 *  ReloadableResourceBundleMessageSource: It also loads message source property file which is reloadable.
	 *  It uses java.util.Properties .
	 * </pre>
	 * 
	 * <pre>
	 *   We either name the method to "messageSource" or add annotation  @Bean(name ="messageSource")
	 * </pre>
	 * <pre>
	 * This is used for getting the internationalization from java code server side.
	 * Like: MessageSource bundle.getMessage(key, null, Locale.US);
	 * And
	 * In ftl pages
	 * Like: <@spring.message 'greeting'/>
	 * The 'greeting' is the key in the usermsg.properties file
	 * </pre>
	 */
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/i18/usermsg");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

}