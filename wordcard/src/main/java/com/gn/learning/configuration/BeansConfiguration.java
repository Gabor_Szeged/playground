package com.gn.learning.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import com.gn.learning.dao.UserDAO;
import com.gn.learning.dao.WordDAO;
import com.gn.learning.dao.impl.UserDAOImpl;
import com.gn.learning.dao.impl.WordDAOImpl;
import com.gn.learning.service.UserService;
import com.gn.learning.service.WordService;
import com.gn.learning.service.impl.UserServiceImpl;
import com.gn.learning.service.impl.WordServiceImpl;
import com.gn.learning.util.UserHelper;

@Configuration
@ComponentScan(basePackages = { "com.gn.learning.*" })
@PropertySource("classpath:/config.properties")
public class BeansConfiguration {

	private static final PropertySourcesPlaceholderConfigurer PROPERTY_SOURCES_PLACEHOLDER_CONFIGURER = new PropertySourcesPlaceholderConfigurer();

	@Bean
	public UserService userService() {
		return new UserServiceImpl();
	}

	@Bean
	public WordService wordService() {
		return new WordServiceImpl();
	}

	@Bean
	public UserDAO userDao() {
		return new UserDAOImpl();
	}

	@Bean
	public WordDAO wordDao() {
		return new WordDAOImpl();
	}

	@Bean
	public UserHelper userHerlper() {
		return new UserHelper();
	}

	// Must register a static PropertySourcesPlaceholderConfigurer bean in
	// either XML or annotation, so that Spring @Value know how to interpret ${}
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
		return PROPERTY_SOURCES_PLACEHOLDER_CONFIGURER;
	}

	// @Bean
	// public CookieLocaleResolver localeResolver() {
	// CookieLocaleResolver resolver = new CookieLocaleResolver();
	// // 1. no cookie for local then it will use the default local and if it
	// // is not set then uses the first possible locale from the request
	// // For example: browser supports: hu, en, en_us then the hu will be
	// // selected. The order of the possible languages can be set in the
	// // browser.
	// // resolver.setDefaultLocale(new Locale("hu_HU"));
	// return resolver;
	// }

	@Bean
	LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
		interceptor.setParamName("lang");
		return interceptor;

	}

}
