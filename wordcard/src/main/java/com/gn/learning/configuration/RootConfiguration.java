package com.gn.learning.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.gn.learning" })
public class RootConfiguration {

	public RootConfiguration() {
//		LogManager.getRootLogger().setLevel(Level.DEBUG);
//		LogManager.getRootLogger().setLevel(Level.OFF);
//		LogManager.getRootLogger().setLevel(Level.OFF);
	}
	
}
