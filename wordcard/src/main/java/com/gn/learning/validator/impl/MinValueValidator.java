package com.gn.learning.validator.impl;

import com.gn.learning.validator.IFieldValidator;

public class MinValueValidator<T extends Comparable<T>> implements IFieldValidator<T> {
	private T maxValue;
	private String errorMessage;

	public MinValueValidator(T minValue) {
		this.maxValue = minValue;
	}

	public boolean isValid(T value) {
		errorMessage = null;
		if (value == null) {
			errorMessage = "Value is missing";
			return false;
		}
		if (value.compareTo(maxValue) <= 0) {
			errorMessage = "Value must be bigger than " + value;
			return false;
		}
		return true;
	}

	@Override
	public String getErrorMessage() {
		return errorMessage;
	}
}