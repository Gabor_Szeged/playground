package com.gn.learning.validator.impl;

import java.util.regex.Pattern;

import com.gn.learning.validator.IFieldValidator;

public class RegexValidator implements IFieldValidator<String> {

	public static final String LATIN1_2_REGEX = "[\\p{L}\\s]+";
	public static final String LATIN1_REGEX = "^[\\p{L1}]*$";
	public static final String ONLY_CHARACTERS_REGEX = "[a-zA-Z]+";
	public static final String EMAIL_FORMAT_REGEX = "^\\S+@\\S+\\.\\S+$";

	private String regex;
	private String errorMessage;

	public RegexValidator(String regex) {
		this.regex = regex;
	}

	public boolean isValid(String value) {
		errorMessage = null;
		if (value == null) {
			errorMessage = "Value is missing";
			return false;
		}
		if (!Pattern.matches(regex, value)) {
			errorMessage = "Format is not valid";
			return false;
		}
		return true;
	}

	@Override
	public String getErrorMessage() {
		return errorMessage;
	}
}