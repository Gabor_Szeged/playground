package com.gn.learning.validator;

public final class FileldLimitations {

	// DB limitations
	public static final int EMAIL_ADDR_MAX_LENGTH = 50;
	public static final int NAME_MAX_LENGTH = 50;
	public static final int MAX_WORD_LENGHT = 40;
	public static final int MAX_WORD_PRONUN = 50;
	public static final int MAX_WORD_DEF = 250;
	public static final int MAX_WORD_USAGE = 500;

	public static final int NAME_MIN_LENGTH = 3;

}
