package com.gn.learning.validator.impl;

import static com.gn.learning.validator.FileldLimitations.NAME_MAX_LENGTH;
import static com.gn.learning.validator.FileldLimitations.NAME_MIN_LENGTH;

import com.gn.learning.validator.IFieldValidator;

public class NameValidator implements IFieldValidator<String> {

	private static final IFieldValidator<Integer> NAME_MAX_LENGTH_VALIDATOR = new MaxValueValidator<Integer>(NAME_MAX_LENGTH);
	private static final IFieldValidator<Integer> NAME_MIN_LENGTH_VALIDATOR = new MinValueValidator<Integer>(NAME_MIN_LENGTH);
	private static final IFieldValidator<String> NAME_CHARACTERS_VALIDATOR = new RegexValidator(RegexValidator.ONLY_CHARACTERS_REGEX);

	private String errorMessage;

	@Override
	public boolean isValid(String value) {
		errorMessage = null;
		if (value == null) {
			errorMessage = "Name is missing";
			return false;
		}
		if (!NAME_MAX_LENGTH_VALIDATOR.isValid(value.length())) {
			errorMessage = "Name is too long. Max length can be: " + NAME_MAX_LENGTH;
			return false;
		}
		if (!NAME_MIN_LENGTH_VALIDATOR.isValid(value.length())) {
			errorMessage = "Name is too short. Min length must be: " + NAME_MIN_LENGTH;
			return false;
		}
		if (!NAME_CHARACTERS_VALIDATOR.isValid(value)) {
			errorMessage = "Name must contains only characters";
			return false;
		}
		return true;
	}

	@Override
	public String getErrorMessage() {
		return errorMessage;
	}

}
