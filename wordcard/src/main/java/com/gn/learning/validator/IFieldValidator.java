package com.gn.learning.validator;

public interface IFieldValidator<T> {

	boolean isValid(T value);
	
	String getErrorMessage();
}
