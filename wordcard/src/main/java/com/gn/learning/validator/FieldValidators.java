package com.gn.learning.validator;

import com.gn.learning.validator.impl.EmailAddressValidator;
import com.gn.learning.validator.impl.NameValidator;

public class FieldValidators {

	public static IFieldValidator<String> getEmailAddressValidator() {
		return new EmailAddressValidator();
	}
	
	
	public static final EmailAddressValidator EMAIL_VALIDATOR =  new EmailAddressValidator();
	
	
	public static IFieldValidator<String> getNameValidator() {
		return new NameValidator();
	}
}
