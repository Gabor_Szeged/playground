package com.gn.learning.validator.impl;

import static com.gn.learning.validator.FileldLimitations.EMAIL_ADDR_MAX_LENGTH;

import com.gn.learning.validator.IFieldValidator;;

public class EmailAddressValidator implements IFieldValidator<String> {

	private static final IFieldValidator<String> EMAIL_FORMAT_VALIDATOR = new RegexValidator(RegexValidator.EMAIL_FORMAT_REGEX);
	private static final IFieldValidator<Integer> EMAIL_LENGTH_VALIDATOR = new MaxValueValidator<Integer>(EMAIL_ADDR_MAX_LENGTH);
	private static final IFieldValidator<String> EMAIL_CHARACTERS_VALIDATOR = new RegexValidator(RegexValidator.LATIN1_REGEX);
	private String errorMessage;

	public boolean isValid(String value) {
		errorMessage = null;
		if (value == null) {
			errorMessage = "Email is missing";
			return false;
		}
		if (!EMAIL_FORMAT_VALIDATOR.isValid(value)) {
			errorMessage = "Email format is not valid.";
			return false;
		}
		if (!EMAIL_LENGTH_VALIDATOR.isValid(value.length())) {
			errorMessage = "Email is too long.";
			return false;
		}
		if (!EMAIL_CHARACTERS_VALIDATOR.isValid(value)) {
			errorMessage = "Email contains not valid character(s).";
			return false;
		}
		return true;
	}

	@Override
	public String getErrorMessage() {

		return errorMessage;
	}
}