package com.gn.learning.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gn.learning.dao.WordDAO;
import com.gn.learning.model.Story;
import com.gn.learning.model.WordCard;
import com.gn.learning.service.WordService;

@Service
public class WordServiceImpl implements WordService {

	@Autowired
	private WordDAO wordDao;

	@Override
	public void addWordCard(WordCard wordCard) {
		wordDao.addWordCard(wordCard);
	}

	@Override
	public List<WordCard> getWordCardsByUserIDAndPackNumberAndStroy(int userID, int packNumber, int storyID) {
		return wordDao.getByUserIDAndPackNumberAndStory(userID, packNumber, storyID);
	}

	@Override
	public void updateWordCard(WordCard wordCard) {
		wordDao.update(wordCard);
	}

	@Override
	public void updateWordCardTimeMovedAndPack(WordCard wordCard) {
		wordDao.updateTimeMovedAndPack(wordCard);
	}

	@Override
	public void updateWordCardConent(WordCard wordCard) {
		wordDao.updateContent(wordCard);
	}

	@Override
	public List<WordCard> getWords(int userID, String word) {
		return wordDao.getWords(userID, word);
	}

	@Override
	public WordCard getWorCardById(int wordID) {
		return wordDao.getByID(wordID);
	}

	@Override
	public List<Story> getStoriesByUser(int userID) {
		return wordDao.getStoriesByUser(userID);
	}

	@Override
	public int getCardsCountByUserIDPackNumberStory(int userId, int packNumber, int storyID) {
		return wordDao.getCardsCountByUserIDPackNumberStory(userId, packNumber, storyID);
	}

	@Override
	public int getStroyId(int userID, String story) {
		return wordDao.getStroyID(userID, story);
	}

	@Override
	public void addStroy(Story story) {
		wordDao.addStory(story);
	}

	@Override
	public String getStoryText(int storyId) {
		return wordDao.getStoryText(storyId);
	}

}
