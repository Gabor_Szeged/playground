package com.gn.learning.service;

import java.util.List;

import com.gn.learning.messages.RegistrationRequest;
import com.gn.learning.model.User;

public interface UserService {

	List<User> selectAllUsers();

	User selectUserById(int id);

	User selectUserByEmail(String email);

	User createUser(RegistrationRequest request);

	/**
	 * Updates only the name and password of the User
	 */
	void update(User user);

	void setValid(User user);

	void delete(int id);

	void setPasswordReset(int userId);
	
}
