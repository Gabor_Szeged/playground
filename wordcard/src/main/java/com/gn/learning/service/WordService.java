package com.gn.learning.service;

import java.util.List;

import com.gn.learning.model.Story;
import com.gn.learning.model.WordCard;

public interface WordService {


	void addWordCard(WordCard convertToWordCard);

	List<WordCard> getWordCardsByUserIDAndPackNumberAndStroy(int userId, int packNumber, int stroyId);

	void updateWordCardTimeMovedAndPack(WordCard wordCard);

	void updateWordCardConent(WordCard wordCard);

	WordCard getWorCardById(int wordId);
	
	void updateWordCard(WordCard wordCard);

	List<WordCard> getWords(int userId, String word);
	
	List<Story> getStoriesByUser(int userId);

	int getCardsCountByUserIDPackNumberStory(int userId, int packNumber, int storyId);

	int getStroyId(int userId, String story);

	void addStroy(Story story);

	String getStoryText(int storyId);

}
