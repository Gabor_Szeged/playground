package com.gn.learning.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gn.learning.dao.UserDAO;
import com.gn.learning.messages.RegistrationRequest;
import com.gn.learning.model.User;
import com.gn.learning.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDAO userDao;
	
	@Override
	public List<User> selectAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User selectUserById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User selectUserByEmail(String email) {
		LOGGER.info("select user by email");
		return userDao.selectByEmail(email);
	}

	@Override
	public User createUser(RegistrationRequest request) {
		LOGGER.info("create new user");
		return userDao.createUser(request.getName(), request.getEmail(), request.getPassword());
	}

	@Override
	public void setPasswordReset(int userId) {
		userDao.setPasswordReset(userId);
	}
	
	@Override
	public void update(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setValid(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
	}

	

}
