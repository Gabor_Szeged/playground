package com.gn.learning.web.model;

/**
 * The “json data” will be converted into this object, via @RequestBody.
 * 
 * @see AjaxController
 * 
 */
public class SearchCriteriaForWords {

	String word;

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

}