package com.gn.learning.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.gn.learning.Constants;
import com.gn.learning.exceptions.MissingUserException;

/***
 * This will assist all known Controllers.
 * 
 * @author Gabor_Nagy1
 *
 */

@ControllerAdvice
public class GlobalExceptionController {

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(Exception ex) {
		ModelAndView model = new ModelAndView(Constants.PAGE_ERROR);
		model.addObject("errMsg", ex.getMessage());
		return model;
	}

	@ExceptionHandler(MissingUserException.class)
	public ModelAndView handleException(MissingUserException ex) {
		ModelAndView model = new ModelAndView(Constants.PAGE_ERROR);
		model.addObject("errMsg", "User was not found. " + ex.getMessage());
		return model;
	}
}
