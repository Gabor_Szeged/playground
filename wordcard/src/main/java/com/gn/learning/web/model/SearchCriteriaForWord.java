package com.gn.learning.web.model;

/**
 * The “json data” will be converted into this object, via @RequestBody.
 * 
 * @see AjaxController
 * 
 */
public class SearchCriteriaForWord {

	int wordId;

	public int getWordId() {
		return wordId;
	}

	public void setWordId(int wordId) {
		this.wordId = wordId;
	}

}