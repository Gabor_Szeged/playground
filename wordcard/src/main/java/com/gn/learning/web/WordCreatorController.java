package com.gn.learning.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.gn.learning.model.Story;
import com.gn.learning.service.WordService;
import com.gn.learning.util.CardUtility;
import com.gn.learning.util.UserHelper;
import com.gn.learning.web.model.AjaxRequestWordBody;
import com.gn.learning.web.model.AjaxResponseBody;

@Controller
@Scope("session")
public class WordCreatorController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordCreatorController.class);

	private static final String PAGE_WORD_CREATOR = "wordCreator";

	@Autowired
	private WordService wordService;

	@Autowired
	private UserHelper userHelper;

	/*
	 * Opens the wordCreator.flt page the first time. It sends the stories collected by the user for the story selector. The word creation will use Ajax.
	 */
	@RequestMapping(value = "/createNewWord", method = RequestMethod.GET)
	public ModelAndView handleGet(HttpServletRequest req) {
		ModelAndView mv = new ModelAndView(PAGE_WORD_CREATOR);

		int userId = -1;
		List<String> storyList = new ArrayList<>();
		try {
			userId = userHelper.getUserId(req);
			if (userId != -1) {
				List<Story> storiesByUser = wordService.getStoriesByUser(userId);
				if (!storiesByUser.isEmpty()) {
					for (Story story : storiesByUser) {
						storyList.add(story.getText());
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Searching for stories faild: {}, excpetion: {}", userId, ex.getMessage());
		}

		mv.addObject("stories", storyList);
		return mv;
	}

	@ResponseBody
	@RequestMapping(value = "/addWord")
	public AjaxResponseBody addWordViaAjax(@RequestBody AjaxRequestWordBody request, HttpServletRequest req) {
		AjaxResponseBody result = new AjaxResponseBody();
		int userId = -1;
		try {
			userId = userHelper.getUserId(req);
			if (userId != -1) {
				String storyName = request.getStory();
				int storyId = wordService.getStroyId(userId, storyName);
				if (storyId == -1 || storyId == 0) {
					// This story is a new one. First add it to the db.
					Story story = new Story();
					story.setText(storyName);
					story.setUserId(userId);
					wordService.addStroy(story);
					storyId = story.getId();
				}

				LOGGER.debug("Add word to user: {}", userId);
				wordService.addWordCard(CardUtility.convertToWordCard(request, userId, storyId));

				result.setCode("200");
				result.setMsg("Result should be fine.");
				LOGGER.debug("Add word to user was OK: {}", userId);
				return result;
			}

		} catch (Exception ex) {
			
			LOGGER.error("Add word to user failed: {}, excpetion: {}", userId, ex.getMessage());
		}

		LOGGER.error("Add word to user failed: {}", userId);
		result.setCode("400");
		result.setMsg("Missing user, request cannot be fullfilled.");
		return result;

	}

}
