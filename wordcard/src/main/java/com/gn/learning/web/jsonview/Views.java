package com.gn.learning.web.jsonview;

/**
 * A dummy class for @JsonView, to control what should be returned back to the
 * request.
 * 
 * @see AjaxController
 */
public class Views {
	public static class Public {
	}
}
