package com.gn.learning.web.model;

import com.gn.learning.model.WordCard;

public class AjaxResponseBodyForWord {

	private WordCard wordCard;

	String code;
	String msg;

	private String storyText;

	public WordCard getWord() {
		return wordCard;
	}

	public void setWordCard(WordCard wordCard) {
		this.wordCard = wordCard;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "AjaxResponseResult [msg=" + msg + "words: " + wordCard + ", code=" + code + "]";
	}

	public String getStoryText() {
		return storyText;
	}

	public void setStoryText(String storyText) {
		this.storyText = storyText;
	}
}
