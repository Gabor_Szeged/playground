package com.gn.learning.web.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;
import com.gn.learning.web.jsonview.Views;

public class AjaxResponseBodyForWords {

	@JsonView(Views.Public.class)
	List<SimpleWordForSearchResult> words = new ArrayList<>();

	@JsonView(Views.Public.class)
	String code;
	@JsonView(Views.Public.class)
	String msg;

	public List<SimpleWordForSearchResult> getWords() {
		return words;
	}

	public void addSimpleWord(SimpleWordForSearchResult sw) {
		words.add(sw);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "AjaxResponseResult [msg=" + msg + "words: " + words + ", code=" + code + "]";
	}
}
