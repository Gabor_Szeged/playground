package com.gn.learning.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gn.learning.Encrpyter;
import com.gn.learning.encrypt.PasswordEncrypterFactory;
import com.gn.learning.model.User;
import com.gn.learning.service.UserService;
import com.gn.learning.util.CardUtility;

@Controller
@Scope("session")
public class ProfileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileController.class);
	private static final String PAGE_PROFILE = "profile";

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView handlePost() {
		ModelAndView mv = new ModelAndView(PAGE_PROFILE);
		String email = CardUtility.getEmailForPrincipal();
		User user = userService.selectUserByEmail(email);
		LOGGER.debug("/profile for {}", user);
		mv.addObject("user", user);
		return mv;
	}

	@RequestMapping(value = "/passwordReset", method = RequestMethod.POST)
	public String handlePasswordReset(@ModelAttribute("oldPassword") String oldPsw,
			@ModelAttribute("newPassword1") String newPsw1, @ModelAttribute("newPassword2") String newPsw2,
			RedirectAttributes redirectAttributes) {
		String email = CardUtility.getEmailForPrincipal();
		User user = userService.selectUserByEmail(email);
		String psw = user.getPassword();
		Encrpyter encrypter = PasswordEncrypterFactory.getEncrypter(PasswordEncrypterFactory.Algorithm.MD5);
		String encryptedOldPsw = encrypter.encrypt(oldPsw);
		if (!psw.equals(encryptedOldPsw)) {
			redirectAttributes.addFlashAttribute("pswMessage", "Old password is not correct.");
		} else if (!newPsw1.equals(newPsw2)) {
			redirectAttributes.addFlashAttribute("pswMessage", "The new passwords do not match.");
		} else {
			String encryptedNewPsw = encrypter.encrypt(newPsw1);
			user.setPassword(encryptedNewPsw);
			userService.update(user);
			redirectAttributes.addFlashAttribute("pswMessage", "Password change was successful.");
		}

		return "redirect:/";
//TODO GN continue with the ftl page
	}

}
