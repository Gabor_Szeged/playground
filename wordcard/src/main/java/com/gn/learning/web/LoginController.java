package com.gn.learning.web;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gn.learning.logic.CardCroupier;
import com.gn.learning.model.User;
import com.gn.learning.util.CardUtility;
import com.gn.learning.util.UserHelper;


@Controller
@Scope("session")
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	// login.ftl
	public static final String PAGE_LOGIN = "login";
	// forgetPassword.ftl
	public static final String PAGE_FORGET_PASSWORD = "forgotPassword";
	// main.ftl
	private static final String PAGE_MAIN = "main";
	private static final String PAGE_UNDER_CONSTRUCTION = "underConstruction";

	// @Autowired
	// private ConversionService conversionService;

	@Autowired
	private UserHelper userHelper;

	@Autowired
	private MessageSource bundle;
	
	/*
	 * This is called when entered: http://localhost:8080/learning/ This page
	 * can be accessed without authentication ".antMatchers("/").permitAll()"
	 * otherwise the ".defaultSuccessUrl("/main")" has no effect. The default
	 * url is NOT used if the user wanted to see a page which requires
	 * authentication because that page will be shown after a successful
	 * authentication.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login() {
		LOGGER.info("/, Login without authetication");
		return PAGE_LOGIN;
	}

	/**
	 * <pre>
	 * Used by the Spring security
	 * http://localhost:8080/learning//login?error
	 * http://localhost:8080/learning//login?logout
	 * </pre>
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView home(@RequestParam(value = "error", required = false) String error, @RequestParam(value = "logout", required = false) String logout,
			HttpServletRequest req) {

		LOGGER.info("/login GET! principal {}.", CardUtility.getEmailForPrincipal());

		ModelAndView mv = new ModelAndView(PAGE_LOGIN);
		if (error != null) {
			LOGGER.info("/login GET! The error is: {} ",  error);
			mv.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			LOGGER.info("/login GET! The logout is: {}.", logout);
			mv.addObject("msg", "You have been looged out successfully.");
		}

		mv.addObject("userName", "");
		mv.addObject("userPsw", "");
		return mv;
	}
	
	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
	public String forgetPassword() {
		return PAGE_FORGET_PASSWORD;
	}

	@RequestMapping(value = "/password-reset", method = RequestMethod.POST)
	public ModelAndView passwordReset(@RequestParam(value = "email") String email, Locale local) {
		ModelAndView mv = new ModelAndView(PAGE_FORGET_PASSWORD);
		User user = userHelper.isEmailregistered(email);
		if (user != null) {
			String message = bundle.getMessage("passwordReset.emailFound", null, local);
			userHelper.setRequestForPaswordReset(user);
			mv.addObject("message", message + " " + email);
		} else {
			String message = bundle.getMessage("passwordReset.emailNotFound", null, local);
			mv.addObject("message", message);
		}
		return mv;
	}
	
	/**
	 * This is the default page after a successful login
	 * ".defaultSuccessUrl("/main")"
	 * 
	 * @param req
	 * @return
	 */
	// http://maruhgar.blogspot.hu/2011/01/converting-from-jsp-to-freemarker.html
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public ModelAndView main(HttpServletRequest req, Locale local) {
		LOGGER.debug("/main ");
		ModelAndView mv = new ModelAndView(PAGE_MAIN);
//		final Locale local2 = new Locale("hu_HU");
//		String message = bundle.getMessage("main.title", null, local2);
//		mv.addObject("title", message);
//		mv.addObject("date", new Date());
//		mv.addObject("message", new TemplateMethodModelEx() {
//			@Override
//			public Object exec(List arguments) throws TemplateModelException {
//				// message('greeting','Hello {0}', 'defaultName')
//				Object object = arguments.get(0);
//				if (object instanceof SimpleScalar) {
//					String key = (String) ((SimpleScalar) object).getAsString();
//					String result = bundle.getMessage(key, null, local2);
//					return result;
//
//				}
//				// String baseContent = (String) arguments.get(1);
//				// String defaultValue = (String) arguments.get(2);
//				return "????";
//			}
//		});
//		mv.addObject("translator", new TemplateMethodModelEx() {
//			@Override
//			public Object exec(List arguments) throws TemplateModelException {
//				Object object = arguments.get(0);
//				if (object instanceof SimpleScalar) {
//					String key = (String) ((SimpleScalar) object).getAsString();
//					String result = bundle.getMessage(key, null, local2);
//					return result;
//				}
//				return "????";
//			}
//		});
		return mv;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		int userID = userHelper.getUserId(request);
		CardCroupier.deletePack(userID);

		CardUtility.logout(request, response);

		LOGGER.debug("/logout");
		// show login screen again.
		return "redirect:/login?logout";
	}

	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("user", CardUtility.getEmailForPrincipal());
		LOGGER.info("Access_Denied GET");
		return "accessDenied";
	}

	@RequestMapping(value = "/Access_Denied", method = RequestMethod.POST)
	public String accessDeniedPage2(ModelMap model) {
		model.addAttribute("user", CardUtility.getEmailForPrincipal());
		LOGGER.info("Access_Denied POST");
		return "accessDenied";
	}

	@RequestMapping(value = "/underConstruction", method = RequestMethod.GET)
	public ModelAndView underConstruction(HttpServletRequest req) {
		LOGGER.debug("/underConstruction ");
		ModelAndView mv = new ModelAndView(PAGE_UNDER_CONSTRUCTION);
		return mv;
	}

	@RequestMapping(value = "/help", method = RequestMethod.GET)
	public String redirect() {
		return "help";
	}

}
