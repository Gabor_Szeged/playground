package com.gn.learning.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gn.learning.model.WordCard;
import com.gn.learning.service.WordService;
import com.gn.learning.util.CardUtility;
import com.gn.learning.util.UserHelper;
import com.gn.learning.web.model.AjaxRequestWordBody;
import com.gn.learning.web.model.AjaxResponseBody;
import com.gn.learning.web.model.AjaxResponseBodyForWord;
import com.gn.learning.web.model.AjaxResponseBodyForWords;
import com.gn.learning.web.model.SearchCriteriaForWord;
import com.gn.learning.web.model.SearchCriteriaForWords;
import com.gn.learning.web.model.SimpleWordForSearchResult;

@Controller
@Scope("session")
public class WordEditorController {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordEditorController.class);

	private static final String PAGE_WORD_EDITOR = "wordEditor";

	@Autowired
	private WordService wordService;
	
	@Autowired
	private UserHelper userHelper;

	@RequestMapping(value = "/gotToEditWord", method = RequestMethod.GET)
	public String handleGet() {
		return PAGE_WORD_EDITOR;
	}

	@ResponseBody
	@RequestMapping(value = "/getSearchResultForWords")
	public AjaxResponseBodyForWords getSearchResultViaAjax(@RequestBody SearchCriteriaForWords search, HttpServletRequest req) {

		AjaxResponseBodyForWords result = new AjaxResponseBodyForWords();

		if (isValidSearchCriteria(search)) {
			int userID = userHelper.getUserId(req);
			LOGGER.debug("Get search result for words [{}] by user: {}", search.getWord(), userID);
			List<WordCard> words = getWords(userID, search.getWord());
			words.sort((wc1, wc2) -> wc1.getWord().compareTo(wc2.getWord()));

			LOGGER.debug("Get search result for words [{}] by user: {}, found: {}", search.getWord(), userID, words.size());
			if (words.size() > 0) {
				result.setCode("200");
				result.setMsg("");
				for (WordCard wordCard : words) {
					result.addSimpleWord(new SimpleWordForSearchResult(wordCard.getId(), wordCard.getWord()));
				}
			} else {
				result.setCode("200");
				result.setMsg("No words was found.");
			}

		} else {
			LOGGER.error("Get search result for words. Wrong search criteria: {}", search.getWord());
			result.setCode("400");
			result.setMsg("Search criteria is empty!");
		}

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/getWordForEditing")
	public AjaxResponseBodyForWord getWordViaAjax(@RequestBody SearchCriteriaForWord search) {

		AjaxResponseBodyForWord result = new AjaxResponseBodyForWord();
		LOGGER.debug("Get word for editing by word: {}", search.getWordId());

		if (search.getWordId() > 0) {
			WordCard wordCard = wordService.getWorCardById(search.getWordId());

			if (wordCard != null) {
				String storyText = wordService.getStoryText(wordCard.getStoryId());
				LOGGER.debug("Found word for editing and its user: {}", wordCard.getUserId());
				result.setWordCard(wordCard);
				result.setStoryText(storyText);
				result.setCode("200");
				result.setMsg("Result should be fine.");
			} else {
				LOGGER.error("Not found word for: {}", search.getWordId());
				result.setCode("204");
				result.setMsg("No word!");
			}

		} else {
			LOGGER.error("Get word for editing. Search criteria is missing.");
			result.setCode("400");
			result.setMsg("Search criteria is empty!");
		}

		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/saveWord")
	public AjaxResponseBody saveWordViaAjax(@RequestBody AjaxRequestWordBody request) {

		AjaxResponseBody result = new AjaxResponseBody();

		WordCard wordCard = CardUtility.convertToWordCard(request);
		LOGGER.debug("Save Word: {}", wordCard.getId());
		wordService.updateWordCardConent(wordCard);

		result.setCode("200");
		result.setMsg("Result should be fine.");

		return result;

	}

	private List<WordCard> getWords(int userId, String word) {
		List<WordCard> wordCards = wordService.getWords(userId, word);
		return wordCards;
	}

	private boolean isValidSearchCriteria(SearchCriteriaForWords search) {
		return search.getWord() != null && search.getWord().length() > 0;
	}

	// @ResponseBody
	// @RequestMapping(value = "updateWord", method = RequestMethod.POST)
	// public String handlePost(WordForm wordCardForm, HttpServletRequest req) {
	// logger.info("word: " + wordCardForm.getWord());
	// logger.info("pronunciation" + wordCardForm.getPronunciation());
	// logger.info("definition" + wordCardForm.getDefinition());
	// logger.info("usage" + wordCardForm.getUsage());
	//
	// int userId = CardUtility.getUserID(req, userService);
	// if (userId != -1) {
	// userService.updateWordCardConent(CardUtility.convertToWordCard(wordCardForm));
	// } else {
	// // TODO show some error
	// }
	// // TODO this will be used by ajax fix it
	// return PAGE_WORD_EDITOR;
	// }

}
