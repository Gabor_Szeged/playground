package com.gn.learning.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gn.learning.logic.CardCroupier;
import com.gn.learning.model.Story;
import com.gn.learning.model.WordCard;
import com.gn.learning.service.WordService;
import com.gn.learning.util.CardUtility;
import com.gn.learning.util.UserHelper;

@Controller
@Scope("session")
public class WordGameContoller {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordGameContoller.class);

	private static final String PAGE_GAME_SELECTOR = "gameSelector";
	private static final String PAGE_WORD_GAME = "wordGame";

	@Autowired
	private WordService wordService;

	@Autowired
	private UserHelper userHelper;

	/*
	 * The value of "story" comes form the comboBox
	 */
	@RequestMapping(value = "/gameSelector", method = RequestMethod.GET)
	public ModelAndView handleGet(HttpServletRequest req,
			@RequestParam(value = "stroy", required = false) String storyText) {
		ModelAndView mv = new ModelAndView(PAGE_GAME_SELECTOR);

		int userId = userHelper.getUserId(req);

		if (userId == -1) {
			return mv;
		}

		/*
		 * At the first opening of the gameSelector.ftl the combo is initialized
		 * the with story names.
		 */
		List<String> storyList = new ArrayList<>();
		try {
			List<Story> storiesByUser = wordService.getStoriesByUser(userId);
			if (!storiesByUser.isEmpty()) {
				for (Story story : storiesByUser) {
					storyList.add(story.getText());
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Searching for stories faild: {}, excpetion: {}", userId, ex.getMessage());
			throw ex;
		}

		mv.addObject("stories", storyList);
		return mv;
	}

	/*
	 * The value of "story" comes form the comboBox on gameSelector.ftl When the
	 * user selected the story. It returns the possible packs, which will be
	 * used on the gameSelector.ftl
	 */
	@RequestMapping(value = "/gameSelector", method = RequestMethod.POST)
	public ModelAndView handlePost2(HttpServletRequest req,
			@RequestParam(value = "storyText", required = false) String storyText) {
		ModelAndView mv = new ModelAndView(PAGE_GAME_SELECTOR);

		int userId = userHelper.getUserId(req);

		if (userId == -1) {
			return mv;
		}

		int storyId = -1;
		/*
		 * Stories for the combo
		 */
		List<String> storyList = new ArrayList<>();
		try {
			List<Story> storiesByUser = wordService.getStoriesByUser(userId);
			if (!storiesByUser.isEmpty()) {
				for (Story story : storiesByUser) {
					if (story.getText().equals(storyText)) {
						storyId = story.getId();
					}
					storyList.add(story.getText());
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Searching for stories faild: {}, excpetion: {}", userId, ex.getMessage());
			throw ex;
		}

		mv.addObject("stories", storyList);

		if (storyText != null && !storyText.isEmpty()) {
			mv.addObject("storyText", storyText);
			mv.addObject("storyId", storyId);
			/*
			 * The user selected a story from the combo box on page
			 * gameSelector.ftl
			 */

			LOGGER.info("/gameSelector, Collect cards for userId: {}", userId);
			int packCount1 = wordService.getCardsCountByUserIDPackNumberStory(userId, 1, storyId);
			int packCount2 = wordService.getCardsCountByUserIDPackNumberStory(userId, 2, storyId);
			int packCount3 = wordService.getCardsCountByUserIDPackNumberStory(userId, 3, storyId);
			int packCount4 = wordService.getCardsCountByUserIDPackNumberStory(userId, 4, storyId);
			int packCount5 = wordService.getCardsCountByUserIDPackNumberStory(userId, 5, storyId);

			if (packCount1 > 0) {
				mv.addObject("pack1", packCount1);
				LOGGER.debug("/gameSelector: pack1 is added");
			}
			if (packCount2 > 0) {
				mv.addObject("pack2", packCount2);
				LOGGER.debug("/gameSelector: pack2 is added");
			}
			if (packCount3 > 0) {
				mv.addObject("pack3", packCount3);
				LOGGER.debug("/gameSelector: pack3 is added");
			}
			if (packCount4 > 0) {
				mv.addObject("pack4", packCount4);
				LOGGER.debug("/gameSelector: pack4 is added");
			}
			if (packCount5 > 0) {
				mv.addObject("pack5", packCount5);
				LOGGER.debug("/gameSelector: pack5 is added");
			}
		}
		return mv;
	}

	/*
	 * The request comes the the "pack" button on the gameSelector.flt page and
	 * this will open the wordGame.ftl page
	 */
	@RequestMapping(value = "/wordGame", method = RequestMethod.POST)
	public ModelAndView handleGameRequestForPack1(@RequestParam(value = "pack1", required = false) String pack1,
			@RequestParam(value = "pack2", required = false) String pack2,
			@RequestParam(value = "pack3", required = false) String pack3,
			@RequestParam(value = "pack4", required = false) String pack4,
			@RequestParam(value = "pack5", required = false) String pack5,
			@RequestParam(value = "storyId", required = true) int storyId, HttpServletRequest req) {

		int userId = userHelper.getUserId(req);
		if (pack1 != null) {
			LOGGER.debug("/wordGame userID: {}, pack num: 1", userId);
			return createModelViewForWordGame(userId, 1, storyId);
		}
		if (pack2 != null) {
			LOGGER.debug("/wordGame userID: {}, pack num: 2", userId);
			return createModelViewForWordGame(userId, 2, storyId);
		}
		if (pack3 != null) {
			LOGGER.debug("/wordGame userID: {}, pack num: 3", userId);
			return createModelViewForWordGame(userId, 3, storyId);
		}
		if (pack4 != null) {
			LOGGER.debug("/wordGame userID: {}, pack num: 4", userId);
			return createModelViewForWordGame(userId, 4, storyId);
		}
		LOGGER.debug("/wordGame userID: {}, pack num: 5", userId);
		return createModelViewForWordGame(userId, 5, storyId);
	}

	private ModelAndView createModelViewForWordGame(int userId, int packNumber, int storyId) {
		ModelAndView mv = new ModelAndView(PAGE_WORD_GAME);

		List<WordCard> pack = wordService.getWordCardsByUserIDAndPackNumberAndStroy(userId, packNumber, storyId);
		String storyText = wordService.getStoryText(storyId);
		mv.addObject("title", "Pack " + packNumber + " (" + pack.size() + ")");
		mv.addObject("storyText", storyText);

		if (pack != null && pack.size() > 0) {
			CardCroupier.storePack(pack, userId);
			WordCard card = CardCroupier.getNextCard(userId);
			if (card != null) {
				mv.addObject("wordCard", card);
			}
		} else {
			LOGGER.debug("/wordGame card not found for pack: {}", packNumber);
			mv.addObject("warning", "There is no card found in this pack. Select another one.");
		}

		return mv;
	}

	private static final String I_KNOW_IT = "1";
	private static final String I_DO_NOT_KNOW_IT = "2";
	private static final String IT_IS_DIFFICULT = "3";

	@RequestMapping(value = "/nextWord", method = RequestMethod.POST)
	public ModelAndView handleNextCardPost(@RequestParam(value = "knownWord", required = false) String toNextPack,
			@RequestParam(value = "cardId", required = true) String cardIdStr,
			@RequestParam(value = "storyText", required = true) String storyText, HttpServletRequest req) {

		ModelAndView mv = new ModelAndView(PAGE_WORD_GAME);
		LOGGER.debug("/nextWord knownWord: {}", toNextPack);

		int userId = userHelper.getUserId(req);
		int cardId = CardUtility.toInt(cardIdStr, -1);
		WordCard oldCard = CardCroupier.getCard(cardId, userId);
		int packNumber = oldCard.getPack();

		LOGGER.debug("/nextWord old card before update: {}", oldCard);

		if (toNextPack != null) {
			// the card goes into the next pack
			if (oldCard.getPack() == 1 && oldCard.isDifficult()) {
				// if the user knows a difficult word then it goes at the
				// beginning of the present pack, so only its moved time is set.
				oldCard.setDifficult(false);
			} else {
				int nextPackNumber = CardCroupier.getNextPackNumber(oldCard.getPack());
				if (nextPackNumber != oldCard.getPack()) {
					CardCroupier.removeCard(userId, oldCard);
					oldCard.setPack(nextPackNumber);
				}
			}
		} else {
			// the card goes into the first pack
			if (oldCard.getPack() != 1) {
				CardCroupier.removeCard(userId, oldCard);
				oldCard.setPack(1);
				oldCard.setDifficult(false);
			} else {
				long timeMoved = CardCroupier.getTimeMovedBack(userId, 10);
				oldCard.setTimeMoved(timeMoved);
				oldCard.setDifficult(true);
			}
		} 

		if (!oldCard.isDifficult()) {
			oldCard.setTimeMoved(System.currentTimeMillis());
		}

		LOGGER.debug("/nextWord save updated card: {}", oldCard);
		wordService.updateWordCardTimeMovedAndPack(oldCard);

		int packSize = CardCroupier.getCardPackSize(userId);
		mv.addObject("title", "Pack " + packNumber + " (" + packSize + ")");

		mv.addObject("storyText", storyText);

		WordCard newCard = CardCroupier.getNextCard(userId);
		LOGGER.debug("/nextWord next card: {}", newCard);
		if (newCard == null) {
			mv.addObject("warning", "There is  no more card. Select a new game.");
		} else {
			mv.addObject("wordCard", newCard);
		}

		return mv;
	}
}
