package com.gn.learning.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gn.learning.messages.RegistrationRequest;
import com.gn.learning.model.User;
import com.gn.learning.service.UserService;
import com.gn.learning.util.CardUtility;

@Controller
@Scope("session")
public class RegistrationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

	private static final String PAGE_REGISTRATION = "registration";

	@Autowired
	private UserService userService;

	/**
	 * The GET request comes from the "login.jsp" clicking on the link and shows
	 * the "register.ftl"
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String register() {
		LOGGER.info("registration GET");
		return PAGE_REGISTRATION;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView register(@ModelAttribute("name") String name, @ModelAttribute("email") String email, @ModelAttribute("password") String password1,
			@ModelAttribute("password2") String password2) {

		ModelAndView mv = new ModelAndView(LoginController.PAGE_LOGIN);
		LOGGER.info("/registration user name: {}, email: {}, password1: {}, password2: {} ", name, email, password1, password2);

		mv.addObject("name", name);
		mv.addObject("email", email);

		String error = CardUtility.checkName(name);
		if (error != null) {
			return setErrorForPageModel(error, PAGE_REGISTRATION, mv);
		}

		error = CardUtility.checkEmailAddress(email);
		if (error != null) {
			return setErrorForPageModel(error, PAGE_REGISTRATION, mv);
		}

		error = CardUtility.checkPasswords(password1, password2);
		if (error != null) {
			return setErrorForPageModel(error, PAGE_REGISTRATION, mv);
		}

		User user = userService.selectUserByEmail(email);
		if (user != null) {
			return setErrorForPageModel("Someone has already registered with this email.", PAGE_REGISTRATION, mv);
		}

		RegistrationRequest request = new RegistrationRequest(name, email, password1);
		User newUser = userService.createUser(request);
		LOGGER.info("/registration,  new user saved: {}", newUser);

		return mv;
	}

	private ModelAndView setErrorForPageModel(String error, String pageName, ModelAndView mv) {
		mv.setViewName(pageName);
		mv.addObject("error", error);
		return mv;
	}
}
