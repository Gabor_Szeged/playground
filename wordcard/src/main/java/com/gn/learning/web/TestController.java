package com.gn.learning.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Scope("session")
public class TestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

	// test.ftl
	public static final String PAGE_TEST = "test";

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String login() {
		LOGGER.info("Login without authetication");
		return PAGE_TEST;
	}
	

}
