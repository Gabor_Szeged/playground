package com.gn.learning.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gn.learning.Constants;
import com.gn.learning.model.User;
import com.gn.learning.service.UserService;

@Component
public class UserHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserHelper.class);

	@Autowired
	private UserService userService;

	/**
	 * Retrieve the userID from the request. If it is not found then gets the user
	 * from the DB by email and set in the userID of the request.
	 * 
	 * @param req
	 * @return
	 */
	public int getUserId(HttpServletRequest req) {
		int userId = -1;
		Object s = req.getSession().getAttribute(Constants.USER_ID);
		if (s instanceof Integer) {
			try {
				userId = (Integer) s;
				LOGGER.debug("userId form session");
			} catch (NumberFormatException e) {
				s = null;
			}
		}

		if (s == null) {
			LOGGER.debug("userId form DB");
			String email = CardUtility.getEmailForPrincipal();
			User user = userService.selectUserByEmail(email);
			if (user == null) {
				String errorMsg = "Not found user for this email: " + email;
				LOGGER.error(errorMsg);
				throw new RuntimeException(errorMsg);
			}
			req.getSession().setAttribute(Constants.USER_ID, user.getID());
			userId = user.getID();
		}
		return userId;

	}
	
	public User isEmailregistered(String email) {
		return userService.selectUserByEmail(email);
	}

	public void setRequestForPaswordReset(User user) {
		
		userService.setPasswordReset(user.getID());
		
//		1. store the email in a separate table because this is not a final solution.
//		2. on the maintenance page dbDumpCreator list all the emails and an imput to change their emails
//		3. send emails to the users with their new passowrd.
//		4. make it possible to change their passwords on their property page
		
	}
}
