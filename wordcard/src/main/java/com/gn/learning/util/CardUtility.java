package com.gn.learning.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import com.gn.learning.model.WordCard;
import com.gn.learning.validator.FieldValidators;
import com.gn.learning.validator.IFieldValidator;
import com.gn.learning.web.model.AjaxRequestWordBody;

public class CardUtility {

	private static final Logger LOGGER = LoggerFactory.getLogger(CardUtility.class);

	private CardUtility() {
	}

	public static String getEmailForPrincipal() {
		String userName = "???";
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null) {
			return userName;
		}
		Authentication authentication = context.getAuthentication();
		if (authentication == null) {
			return userName;
		}
		LOGGER.debug("Auth: {}", authentication);
		Object principal = authentication.getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		LOGGER.debug("Auth: {}", userName);
		return userName;
	}

	public static String checkPasswords(String password, String password2) {
		if (password.isEmpty() || password2.isEmpty() || !password.equals(password2)) {
			return "Passwords are not equal or empty";
		}
		if (password.length() < 6) {
			return "Password must be at least 6 character long";
		}
		return null;
	}

	public static String checkEmailAddress(String email) {
		IFieldValidator<String> validator = FieldValidators.getEmailAddressValidator();
		validator.isValid(email);
		return validator.getErrorMessage();
	}

	public static String checkName(String name) {
		IFieldValidator<String> validator = FieldValidators.getNameValidator();
		validator.isValid(name);
		return validator.getErrorMessage();
	}

	public static void logout(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
	}

	public static int toInt(String packNumber, int def) {
		try {
			return Integer.parseInt(packNumber);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	public static WordCard convertToWordCard(AjaxRequestWordBody request) {
		WordCard wc = new WordCard();
		wc.setId(request.getId());
		wc.setWord(request.getWord());
		wc.setPronunciation(request.getPronunciation());
		wc.setDefinition(request.getDefinition());
		wc.setUsages(request.getUsages());
		return wc;
	}

	public static WordCard convertToWordCard(AjaxRequestWordBody request, int userId, int storyId) {
		WordCard wc = new WordCard();
		wc.setUserId(userId);
		wc.setStoryId(storyId);
		wc.setWord(request.getWord());
		wc.setPronunciation(request.getPronunciation());
		wc.setDefinition(request.getDefinition());
		wc.setUsages(request.getUsages());
		wc.setPack((byte) 1);
		wc.setTimeMoved(new Date().getTime());
		return wc;
	}

}
