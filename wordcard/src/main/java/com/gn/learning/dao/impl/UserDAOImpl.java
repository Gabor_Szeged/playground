package com.gn.learning.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gn.learning.dao.UserDAO;
import com.gn.learning.model.Role;
import com.gn.learning.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@Override
	public List<User> selectAll() {
		List<User> list = null;
		try (SqlSession session = sqlSessionFactory.openSession()) {
			list = session.selectList("User.selectAll");
		}
		return list;
	}

	@Override
	public User selectById(int id) {
		User user = null;
		try (SqlSession session = sqlSessionFactory.openSession()) {
			user = (User) session.selectOne("User.selectById", id);
		}
		System.out.println("selectById(" + id + ") --> " + user);
		return user;
	}

	@Override
	public User selectByEmail(String email) {
		User user = null;
		try (SqlSession session = sqlSessionFactory.openSession()) {
			user = (User) session.selectOne("User.selectByEmail", email);
		}
		System.out.println("selectByEmail(" + email + ") --> " + user);
		return user;
	}

	@Override
	public User createUser(String name, String email, String password) {
		User user = new User();
		user.setName(name);
		user.setEmail(email);
		user.setPassword(password);
		user.setDateOfRegistration(new Date());
		/*
		 * TODO until there is no Email confirmation this value is set true
		 */
		user.setValidUser(true);

		List<String> resultList;
		try (SqlSession session = sqlSessionFactory.openSession()) {
			session.insert("User.insert", user);

			Role role = new Role();
			role.setEmail(email);
			role.setRole(Role.USER);
			session.insert("Role.insert", role);
			resultList = session.selectList("Role.selectByEmail", email);
			session.commit();
		}

		String role = resultList.size() > 0 ? (String) resultList.get(0) : "Missing role";
		logger.info("insert(" + user + ") --> role: " + role);
		return user;
	}

	@Override
	public void update(User user) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			// User.xml <update id="update" ...
			session.update("User.update", user);
			session.commit();
		}
	}

	@Override
	public void setValid(User user) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			// User.xml <update id="update" ...
			session.update("User.setValid", user);
			session.commit();
		}
	}

	@Override
	public void delete(int id) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			// User.xml <delete id="delete" ...
			session.delete("User.delete", id);
			session.commit();
		}
	}

	@Override
	public void setPasswordReset(int userId) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			session.update("User.setPasswordReset", userId);
			session.commit();
		}
		logger.info("setPasswordReset(" + userId + ")");
	}
	
}
