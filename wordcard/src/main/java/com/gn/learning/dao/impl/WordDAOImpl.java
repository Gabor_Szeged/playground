package com.gn.learning.dao.impl;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.gn.learning.dao.WordDAO;
import com.gn.learning.model.Story;
import com.gn.learning.model.WordCard;

public class WordDAOImpl implements WordDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(WordDAOImpl.class);

	@Autowired
	private SqlSessionFactory sqlSessionFactory;

	@Override
	public void addWordCard(WordCard wordCard) {
		createAndRunDBProcess("WordCard.insert", wordCard);
	}

	@Override
	public void update(WordCard wordCard) {
		createAndRunDBProcess("WordCard.update", wordCard);
	}

	@Override
	public void updateContent(WordCard wordCard) {
		createAndRunDBProcess("WordCard.updateContent", wordCard);
	}

	@Override
	public void updateTimeMovedAndPack(WordCard wordCard) {
		createAndRunDBProcess("WordCard.updateTimeMovedAndPack", wordCard);
	}
	
	@Override
	public void addStory(Story story) {
		createAndRunDBProcess("WordCard.addStory", story);
	}

	@Override
	public int getCardsCountByUserIDPackNumberStory(int userId, int packNumber, int storyId) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			Map<String, Integer> map = new Hashtable<>();
			map.put("userId", userId);
			map.put("packNumber", packNumber);
			map.put("stroyId", storyId);
			Integer cardCound = session.selectOne("getCardsCountByUserIDPackNumberStory", map);
			LOGGER.info("Cards numbers for  UserId:{}, packNumber:{}, storyId:{} ", userId, packNumber, storyId);
			return cardCound.intValue();
		}
	}

	@Override
	public List<WordCard> getByUserIDAndPackNumberAndStory(int userId, int packNumber, int storyId) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			Map<String, Integer> map = new Hashtable<>();
			map.put("userId", userId);
			map.put("packNumber", packNumber);
			map.put("stroyId", storyId);
			List<WordCard> resultList = session.selectList("selectWordCardsByUserIdAndPackAndStroy", map);
			if (resultList != null && resultList.size() > 0) {
				LOGGER.info("Cards found for  UserId:{}, packNumber:{}, storyId:{} ", userId, packNumber, storyId);
				for (WordCard wordCard : resultList) {
					LOGGER.debug("++++ CARD{}: ", wordCard);
				}
			} else {
				LOGGER.info("No Cards found for  UserId:{}, packNumber:{}, storyId:{} ", userId, packNumber, storyId);
			}
			return resultList;
		}
	}

	@Override
	public List<WordCard> getWords(int userId, String word) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			word = word.replace('*', '%');
			word = word.replace('.', '_');
			Map<String, String> map = new Hashtable<>();
			map.put("userId", "" + userId);
			map.put("word", word);
			List<WordCard> resultList = session.selectList("selectWords", map);
			if (resultList != null && resultList.size() > 0) {
				for (WordCard wordCard : resultList) {
					LOGGER.info("++++ CARD: " + wordCard);
				}
			} else {
				LOGGER.info("++++ No card foud by word: " + word);
			}
			return resultList;
		}
	}

	@Override
	public WordCard getByID(int wordId) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			WordCard wordCard = session.selectOne("selectWordById", wordId);
			return wordCard;
		}
	}

	private void createAndRunDBProcess(String sqlCommand, WordCard wordCard) {
		DBProcess process = new DBProcess() {
			@Override
			public void exexute(SqlSession session) {
				session.insert(sqlCommand, wordCard);
			}
		};
		runDBProcess(process);
	}
	
	private void createAndRunDBProcess(String sqlCommand, Story story) {
		DBProcess process = new DBProcess() {
			@Override
			public void exexute(SqlSession session) {
				session.insert(sqlCommand, story);
			}
		};
		runDBProcess(process);
	}

	private void runDBProcess(DBProcess process) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			process.exexute(session);
			session.commit();
		}
	}

	private class DBProcess {
		public void exexute(SqlSession session) {
		}
	}

	@Override
	public List<Story> getStoriesByUser(int userId) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			Map<String, Integer> map = new Hashtable<>();
			map.put("userId", userId);
			List<Story> resultList = session.selectList("selectStoriesByUser", map);
			
			if (resultList != null && resultList.size() > 0) {
				LOGGER.info("Stories found for  UserId:{}", userId);
				for (Story story : resultList) {
					LOGGER.debug("++++ Story{}: ", story);
				}
			} else {
				LOGGER.info("No Stories found for  UserId:{}", userId);
			}
			return resultList;
		}
	}

	@Override
	public int getStroyID(int userId, String storyText) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			Story story = new Story();
			story.setUserId(userId);
			story.setText(storyText);
			Integer storyId = session.selectOne("getStoryIDByUserAndStory", story);
			LOGGER.info("StoryID: {} for  UserId:{}, stroy:{} ",storyId, userId, story);
			if (storyId == null) {
				return -1;
			}
			return storyId.intValue();
		}
	}

	@Override
	public String getStoryText(int storyId) {
		try (SqlSession session = sqlSessionFactory.openSession()) {
			String storyText = session.selectOne("getStoryTextByStoryId", storyId);
			LOGGER.info("StoryText by storyId:{} stroyText:{} ",storyId, storyText);
			return storyText;
		}
	}


	// @Override
	// public void update(WordCard wordCard) {
	// DBProcess<Integer> process = new DBProcess<Integer>() {
	//
	// @Override
	// public <Ret> Ret exexute(SqlSession session, Ret unusedTypeObj) {
	// int i = session.insert("WordCard.update", wordCard);
	// return (Ret) Integer.valueOf(i);
	// };
	// };
	// Integer result = runDBProcess(process, Integer.class);
	// }
	//
	// public void updateTimeMovedAndPack(WordCard wordCard) {
	// DBProcess<Integer> process = new DBProcess<Integer>() {
	//
	// @Override
	// public <Ret> Ret exexute(SqlSession session, Ret unusedTypeObj) {
	// int i = session.insert("WordCard.updateTimeMovedAndPack", wordCard);
	// return (Ret) Integer.valueOf(i);
	// };
	// };
	// Integer result = runDBProcess(process, Integer.class);
	// }
	//
	// public <Ret extends Object> Ret runDBProcess(DBProcess<?> process, Ret
	// unusedTypeObj) {
	// SqlSession session = sqlSessionFactory.openSession();
	// try {
	// return process.exexute(session, unusedTypeObj);
	// } finally {
	// session.close();
	// }
	// }
	//
	// private class DBProcess <Ret>{
	// public <Ret extends Object> Ret exexute(SqlSession session, Ret
	// unusedTypeObj ) {
	// return null;
	// }
	// }

}
