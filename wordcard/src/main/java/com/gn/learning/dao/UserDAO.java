package com.gn.learning.dao;

import java.util.List;

import com.gn.learning.model.User;

public interface UserDAO {

	List<User> selectAll();

	User selectById(int id);

	User selectByEmail(String email);

	User createUser(String name, String email, String password);

	/**
	 * Updates only the name and password of the User
	 */
	void update(User user);

	void setValid(User user);

	void delete(int id);

	void setPasswordReset(int userId);
}
