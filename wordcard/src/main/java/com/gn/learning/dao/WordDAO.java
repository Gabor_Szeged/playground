package com.gn.learning.dao;

import java.util.List;

import com.gn.learning.model.Story;
import com.gn.learning.model.WordCard;

public interface WordDAO {

	void addWordCard(WordCard wordCard);

	List<WordCard> getByUserIDAndPackNumberAndStory(int userID, int packNumber, int storyId);

	void update(WordCard wordCard);

	void updateTimeMovedAndPack(WordCard wordCard);

	void updateContent(WordCard wordCard);

	List<WordCard> getWords(int userId, String word);

	WordCard getByID(int wordId);

	List<Story> getStoriesByUser(int userId);

	int getCardsCountByUserIDPackNumberStory(int userID, int packNumber, int storyId);

	int getStroyID(int userID, String story);

	void addStory(Story story);

	String getStoryText(int storyId);
}
