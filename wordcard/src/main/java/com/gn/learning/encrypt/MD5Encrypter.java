package com.gn.learning.encrypt;

import org.springframework.util.DigestUtils;

import com.gn.learning.Encrpyter;

public class MD5Encrypter implements Encrpyter {

	@Override
	public String encrypt(String password) {
		return DigestUtils.md5DigestAsHex(password.getBytes());
	}

}
