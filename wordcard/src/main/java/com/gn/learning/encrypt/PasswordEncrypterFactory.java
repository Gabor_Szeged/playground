package com.gn.learning.encrypt;

import com.gn.learning.Encrpyter;

public class PasswordEncrypterFactory {

	public static enum Algorithm {MD5, NONE}

	private static Encrpyter md5PasswordEncrypter = new MD5Encrypter();
	private static Encrpyter sterileEncrypter = new DummyEncrypter();

	public static Encrpyter getEncrypter(Algorithm encrypterAlgorithm) {
		switch (encrypterAlgorithm) {
		case MD5:
			return md5PasswordEncrypter;
		case NONE:
			return sterileEncrypter;
		default:
			throw new RuntimeException("Encrypter algorithm is not supported: " + encrypterAlgorithm);
		}
	}

}
