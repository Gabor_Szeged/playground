package com.gn.learning.model;

public class Role {

	public static final String USER = "ROLE_USER";
	public static final String ADMIN = "ROLE_ADMIN";

	private String email;
	private String role;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
