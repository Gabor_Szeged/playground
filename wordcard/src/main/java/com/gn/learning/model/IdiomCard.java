package com.gn.learning.model;

import java.util.Date;

public class IdiomCard {

	private int id;
	private int userId;
	private byte pack;
	private String idiom;
	private String keyWord;
	private String phonetics;
	private String usage;
	private Date timeMoved;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public byte getPack() {
		return pack;
	}

	public void setPack(byte pack) {
		this.pack = pack;
	}

	public String getIdiom() {
		return idiom;
	}

	public void setIdiom(String idiom) {
		this.idiom = idiom;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getPhonetics() {
		return phonetics;
	}

	public void setPhonetics(String phonetics) {
		this.phonetics = phonetics;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public Date getTimeMoved() {
		return timeMoved;
	}

	public void setTimeMoved(Date timeMoved) {
		this.timeMoved = timeMoved;
	}

}
