package com.gn.learning.model;

public class WordCard {

	private int id;
	private int userId;
	private int storyId;
	private String word;
	private String pronunciation;
	private String usages;
	private String definition;
	private int pack;
	private long timeMoved;
	private boolean difficult;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getStoryId() {
		return storyId;
	}

	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getPronunciation() {
		return pronunciation;
	}

	public void setPronunciation(String pronunciation) {
		this.pronunciation = pronunciation;
	}

	public String getUsages() {
		return usages;
	}

	public void setUsages(String usages) {
		this.usages = usages;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public int getPack() {
		return pack;
	}

	public void setPack(int pack) {
		this.pack = pack;
	}

	public long getTimeMoved() {
		return timeMoved;
	}

	public void setTimeMoved(long timeMoved) {
		this.timeMoved = timeMoved;
	}
	
	public boolean isDifficult() {
		return difficult;
	}

	public void setDifficult(boolean difficult) {
		this.difficult = difficult;
	}

	@Override
	public String toString() {
		return "id: " + id + ", userId: " + userId + ", pack: " + pack + ", word: " + word + ", def: " + definition
				+ ", usage: " + usages;
	}

}
