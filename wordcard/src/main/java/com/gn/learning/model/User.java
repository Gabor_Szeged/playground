package com.gn.learning.model;

import java.util.Date;

public class User {

	private int id;
	private String name;
	private String email;
	private String password;
	private boolean validUser;
	private Date dateOfRegistration;

	public int getID() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String psw) {
		this.password = psw;
	}

	public String getPassword() {
		return password;
	}

	public boolean isValidUser() {
		return validUser;
	}

	public void setValidUser(boolean validUser) {
		this.validUser = validUser;
	}

	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}

	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	@Override
	public String toString() {
		return String.format("id: %d, name: %s, email: %s, psw: %s, validUser: %s, daget of reg.: %s", getID(),
				getName(), getEmail(), getPassword(), isValidUser(), getDateOfRegistration());
	}

}
