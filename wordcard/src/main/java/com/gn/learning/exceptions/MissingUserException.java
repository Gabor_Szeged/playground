package com.gn.learning.exceptions;

public class MissingUserException extends Exception {

	private static final long serialVersionUID = 1L;

	public MissingUserException(String message) {
		super(message);
	}

}
