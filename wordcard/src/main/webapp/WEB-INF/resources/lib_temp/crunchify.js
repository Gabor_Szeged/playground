
var hintCounter = 1;
function toggle() {
	if (hintCounter < 5) {
		$('#activeText' + hintCounter).toggle();
		hintCounter = hintCounter + 1;
	} else {
		hintCounter = 1;
	}
}  

function hideTexts() {
	 $('#activeText1').toggle();
	 $('#activeText2').toggle();
	 $('#activeText3').toggle();
	 $('#activeText4').toggle();
}








jQuery(document).ready(function($) {
 
	$('#crunchifyMessage').html("<h4>This message is coming from 'crunchify.js' file...</h4>")
 
});

function doAjaxPost() 
{
	alert("doAjaxPost was called");
}


function toggle1() {
	var element = document.getElementById('activeText');
	
	if (element.style.display != 'none') {
		element.style.display='none';
	} else {
		element.style.display='';
	}
}

jQuery(document).ready(function($) {

	$("#search-form").submit(function(event) {

		// Disble the search button
		enableSearchButton(false);

		// Prevent the form from submitting via the browser.
		event.preventDefault();

		searchViaAjax();

	});

});

function searchViaAjax() {

	var search = {}
	search["username"] = $("#username").val();
	search["email"] = $("#email").val();
	
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");

	var headers = {};
    headers[csrfHeader] = csrfToken;
	
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		url : "./getSearchResult",
		headers: headers,
		data : JSON.stringify(search),
		dataType : 'json',
		timeout : 100000,
		cache : false,
		
		success : function(data) {
			console.log("SUCCESS: ", data);
			display(data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
			display(e);
			display(e);
		},
		done : function(e) {
			console.log("DONE");
			enableSearchButton(true);
		}
	});

}

function enableSearchButton(flag) {
	$("#btn-search").prop("disabled", flag);
}

function display(data) {
	var json = "<h4>Ajax Response</h4><pre>"
			+ JSON.stringify(data, null, 4) + "</pre>";
	$('#feedback').html(json);
}

function doAjaxPost2() 
{
    var firstName= $('#firstName').val();
    var lastName= $('#lastName').val();



    var json = {"firstName" : firstName, "lastName" : lastName};
    console.log(json);

    var variableFreeMarker = "${freeMarkValue}";
    console.log('this value is: ' + variableFreeMarker); 

    $.ajax(
    {
        type: "POST",
        url: "myUrl",
        data: JSON.stringify(json), 

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,

        beforeSend: function(xhr) 
                        {
                        xhr.setRequestHeader("Accept", "application/json");  
                        xhr.setRequestHeader("Content-Type", "application/json");  
                        },
        success: function(data) 
                {

                },
            error:function(data,status,er) { 
                alert("error: "+data+" status: "+status+" er:"+er);
            }
        /* error: function (xhr, ajaxOptions, thrownError) 
               {
                    alert(xhr.status);
                    alert(xhr.responseText);
                    alert(thrownError);
        }*/
    });
}