var pronunCharPos;

jQuery(document).ready(function() {
	loadBundles("en");
	
	
	// Keep track of the insertion position of the special characters
	$("#pronunciation").bind("keydown keypress mousemove", function() {
		setTimeout(function() {
		  pronunCharPos = $("#pronunciation").caret();
		}, 100);
	});	
	
	toggleFields();

	 $("#storyNameSelector").change(function () {
	        toggleFields();
	 });
	
});

// this toggles the visibility of other story
function toggleFields() {
	if ($("#storyNameSelector").val() === "other") {
		$("#newStoryDiv").show();
		$("#storyName").val('');
	}
	else {
		$("#newStoryDiv").hide();
		var v =  $('#storyNameSelector').find(":selected").text();
		$("#storyName").val(v);
	}
}

function loadBundles(lang) {
	jQuery.i18n.properties({
		name : 'Messages',
		path : 'resources/lib/',
		mode : 'both',
		callback : function() {
		}
	});
}

jQuery(document).ready(function($) {
	$("#addWord-form").submit(function(event) {
		disableSaveButton(true);
		event.preventDefault();
		saveWordViaAjax();
	});
});


function storySelectedFromList() {
	var story = $("#storyNameSelector").val();
	$("#storyName").val(story);
}

function removeSelected() {
	$("#storyName").val('');
}

function saveWordViaAjax() {

	var savedWord = $("#word").val();
	var request = {}
	request["story"] = $("#storyName").val();
	request["word"] = savedWord;
	request["pronunciation"] = $('#pronunciation').val();
	request["definition"] = $('#definition').val();
	request["usages"] = $('#usage').val();

	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");

	var headers = {};
	headers[csrfHeader] = csrfToken;

	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		url : "./addWord",
		headers : headers,
		data : JSON.stringify(request),
		dataType : 'json',
		timeout : 15000,
		cache : false,

		success : function(data) {
			resetEditor();
			console.log("SUCCESS: ", data);
			disableSaveButton(false);
			displayOK(jQuery.i18n.prop('last_save_word') + " " + savedWord);
		},
		error : function(e) {
			console.log("ERROR: ", e);
			display(e);
		},
		done : function(e) {
			console.log("DONE");
			display(e);
		}
	});
}

function disableSaveButton(flag) {
	// $("#btn-save").prop("disabled", flag);
	if (flag) {
		$("#btn-save").prop("value", "Saving...");
	} else {
		$("#btn-save").prop("value", "Save");
	}
}

function resetEditor() {
	$('#word').val('');
	$('#pronunciation').val('');
	$('#definition').val('');
	$('#usage').val('');
}

function insertCharToPronun(specChar) {
	var temp = $('#pronunciation').val();
	temp = temp.insert(pronunCharPos, specChar);
	pronunCharPos++;
	$('#pronunciation').val(temp);
}

String.prototype.insert = function(idx, str) {
	return this.slice(0, idx) + str + this.slice(idx, this.length);
}

function display(data) {
	var json = "<h4>Ajax Response</h4><pre>" + JSON.stringify(data, null, 4)
			+ "</pre>";
	$('#feedback').html(json);
}

function displayOK(message) {
	var json = "<h4>" + message + "</h4>";
	$('#feedback').html(json);
}

