var pronunCharPos;

jQuery(document).ready(function() {
	loadBundles("en");
	
	// Keep track of the insertion position of the special characters
	$("#pronunciation").bind("keydown keypress mousemove", function() {
		setTimeout(function() {
		  pronunCharPos = $("#pronunciation").caret();
		}, 100);
	});	
});

function loadBundles(lang) {
	jQuery.i18n.properties({
		name : 'Messages',
		path : 'resources/lib/',
		mode : 'both',
		callback : function() {
		}
	});
}

jQuery(document).ready(function($) {

	$("#search-form").submit(function(event) {

		// Disble the search button
		disableSearchButton(true);

		// Prevent the form from submitting via the browser.
		event.preventDefault();

		searchViaAjax();

	});
	
	$("#save-form").submit(function(event) {
		disableSaveButton(true);
		event.preventDefault();
		saveWordViaAjax();
	});
});

function searchViaAjax() {

	var search = {}
	search["word"] = $("#searchText").val();

	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");

	var headers = {};
	headers[csrfHeader] = csrfToken;

	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		url : "./getSearchResultForWords",
		headers : headers,
		data : JSON.stringify(search),
		dataType : 'json',
		timeout : 15000,
		cache : false,

		success : function(data) {
			console.log("SUCCESS: ", data);
			intoList(data);
			disableSearchButton(false);
		},
		error : function(e) {
			console.log("ERROR: ", e);
			display(e);
			display(e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function getWordViaAjax(wordId) {
	
	var search = {}
	search["wordId"] = wordId;
	
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	
	var headers = {};
	headers[csrfHeader] = csrfToken;
	
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		url : "./getWordForEditing",
		headers : headers,
		data : JSON.stringify(search),
		dataType : 'json',
		timeout : 15000,
		cache : false,
		
		success : function(data) {
			console.log("SUCCESS: ", data);
			intoEditor(data);
		},
		error : function(e) {
			console.log("ERROR: ", e);
			display(e);
			display(e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function saveWordViaAjax() {
	
	var savedWord = $("#word").val();
	var request = {}
	request["id"] = $("#wordId").val();
	request["word"] = savedWord;
	request["pronunciation"] = $('#pronunciation').val();
	request["definition"] = $('#definition').val();
	request["usages"] = $('#usage').val();
	
	
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	
	var headers = {};
	headers[csrfHeader] = csrfToken;
	
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		url : "./saveWord",
		headers : headers,
		data : JSON.stringify(request),
		dataType : 'json',
		timeout : 15000,
		cache : false,
		
		success : function(data) {
			searchViaAjax();
			disableSaveButton(false);
			displayOK(jQuery.i18n.prop('last_save_word') + " " + savedWord);
		},
		error : function(e) {
			console.log("ERROR: ", e);
			display(e);
		},
		done : function(e) {
			console.log("DONE");
		}
	});
}

function disableSearchButton(flag) {
	$("#btn-search").prop("disabled", flag);
}

function disableSaveButton(flag) {
	$("#btn-save").prop("disabled", flag);
}

function intoList(data) {
	var code = data.code;
	var msg = data.msg;

	var words = data.words;
	var listElements = "";
	var counter = 1;
	for ( var word in data.words) {
		var elem = data.words[word];
		listElements += "<li onclick=\"getWord("+elem.id+")\">" + elem.word + "</li>";
		counter++;
	}

	var htmlWordList = " <ul> " + listElements + "</ul>";
	$('#list-container').html(htmlWordList);
}

function getWord(wordId) {
	getWordViaAjax(wordId);
}

function intoEditor(data) {
//	display(data);
	
	$('#wordId').val(data.word.id);
	$('#pack').val(data.word.pack);
	$('#story').val(data.storyText);
	$('#word').val(data.word.word);
	$('#pronunciation').val(data.word.pronunciation);
	$('#definition').val(data.word.definition);
	$('#usage').val(data.word.usages);
}

function insertCharToPronun(specChar) {
	var temp = $('#pronunciation').val();
	temp = temp.insert(pronunCharPos, specChar);
	pronunCharPos++;
	$('#pronunciation').val(temp);
}

String.prototype.insert = function(idx, str) {
	return this.slice(0, idx) + str + this.slice(idx, this.length);
}

function display(data) {
	var json = "<h4>Ajax Response</h4><pre>" + JSON.stringify(data, null, 4) + "</pre>";
	$('#feedback').html(json);
}

function displayOK(message) {
	var json = "<h4>" + message + "</h4>";
	$('#feedback').html(json);
}