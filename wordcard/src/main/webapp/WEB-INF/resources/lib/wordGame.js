
var activeTextFields = ['#usage', '#pronunciation', '#word'];
var hintCounter = 1;

function toggle() {
	if (hintCounter == 1){
		$('#usage').toggle();
	} else if (hintCounter == 2){
		$('#pronunciation').toggle();
		$('#word').toggle();
	}
	hintCounter++;
	if (hintCounter == 3){
		$('#hintButton').toggle();
		$('#bt-next').toggle();
	}
}  

function hideTexts() {
	 $('#word').toggle();
	 $('#pronunciation').toggle();
	 $('#usage').toggle();
	 $('#bt-next').toggle();
}

