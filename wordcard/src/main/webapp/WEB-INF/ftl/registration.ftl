<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>
 <head>
  <link rel="stylesheet" type="text/css" href="resources/css/registration.css" />
  <link rel="stylesheet" type="text/css" href="resources/css/divTable.css" />
  <title><@spring.message 'registration.headerTitle'/></title>
 </head>


<body>

<div id="divMain">
	  <div id="divRegistrationTitle">
         <h1 align="center"><@spring.message 'registration.title'/></h1>
      </div>

	  <div id="divRegistrationFrame">
		<#if error??>
			<font size="3" color="red">${error}</font>
		</#if>
		<fieldset>
		<!-- 
		  <legend>Add User</legend>
		 -->
		 
		    <!-- the value of the "action" is used in @RequestMapping(value = "/registration",...) -->
			<form action="registration" method="POST">
				<div class="divTable">
					<div class="divTableBody">
						<div class="divTableRow">
							<div class="divTableCell"><@spring.message 'registration.userName'/></div>
							<div class="divTableCell">
								<#if name??>
									<input type="text" name="name" id="name" value=${name}>
								<#else>
									<input type="text" name="name" id="name"/>
								</#if>
							</div>
						</div>
						<div class="divTableRow">
							<div class="divTableCell"><@spring.message 'registration.email'/></div>
							<div class="divTableCell">
								<#if email??>
									<input type="text" name="email" id="email" value=${email}>
								<#else>
									<input type="text" name="email" id="email"/>
								</#if>
							</div>
						</div>
						<div class="divTableRow">
							<div class="divTableCell"><@spring.message 'registration.password'/></div>
							<div class="divTableCell"><input type="password" name="password" /></div>
						</div>
						<div class="divTableRow">
							<div class="divTableCell"><@spring.message 'registration.password'/></div>
							<div class="divTableCell"><input type="password" name="password2" /></div>
						</div>
						<div class="divTableRow">
							<div class="divTableCell"></div>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<div class="divTableCell">
							  <div class="right">
							    <input type="submit" value="Register" />
							  </div>
							</div>
						</div>
					</div><!-- divTableBody -->
				</div><!-- divTable -->
			</form>
		</fieldset>
	</div>
  </div>

</body>
</html>
