<!DOCTYPE html>
<html>

<head>
 <link rel="stylesheet" type="text/css" href="resources/css/error_page.css">
 <title>ERROR</title>
</head>

<body>

<h1>!!! Sorry a problem has occurred. </h1>

<#if errMsg??>
	<h2>Message: </h2> ${errMsg}
</#if>

	
<div id="linkToMainPage">
	<a href="main">To Main page</a>
</div>
	
</body>
</html>
