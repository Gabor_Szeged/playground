<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>

<head>
  <script type="text/javascript" src="resources/lib/jquery-2.2.1.min.js"></script>
  <script type="text/javascript" src="resources/lib/gameSelector.js"></script>
  <link rel="stylesheet" type="text/css" href="resources/css/game_selector.css">
  <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">
  <title><@spring.message 'gameSelector.headerTitle'/></title>
</head>

<body>

	<div id="common-container">
		<a href="main"><@spring.message 'common.toMainPage'/></a>
		<a href="help"><@spring.message 'common.help'/></a>
		<a href="logout"><@spring.message 'common.logout'/></a>
  	</div>

  <div class="center">
     <h1><@spring.message 'gameSelector.title'/></h1>
  </div>
 
 
 <div id="selector-container">
    <div id="story-container">
	 	<form action="gameSelector" method="POST">
    		<div id="storySelectorName-container">
    		 	<@spring.message 'gameSelector.stories'/>
    	 	</div>
		 	<div id="storySelector-container">
				<select name="storyText" >
			    	<option value="">...</option>
					<#list stories as storyName>
						<option value="${storyName}">${storyName}</option>
					</#list>
				</select>
				<#if storyText??>
			    	${storyText}
				</#if>
        		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<input type="submit" name="select" value="Select" />
			</div>
			<div class="clearfix"></div>
		</form>
    </div> <!--story-container-->
    
  <div class="divTable">
	<div class="divTableBody">
		<div class="divTableRow">
			<div class="divTableCell">
					<h3 id=words-title><@spring.message 'gameSelector.words'/></h3>
			</div>
			<#if pack1??>
			<div class="divTableCell">
				<form action="wordGame" method="POST">
					  <input type="hidden" name="storyId" value="${storyId}"/>
					  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					  <input type="submit" title=${pack1} name="pack1" value="Pack 1" />
				</form>
			</div>
			</#if>
			<#if pack2??>
			<div class="divTableCell">
				<form action="wordGame" method="POST">
					  <input type="hidden" name="storyId" value="${storyId}"/>
					  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					  <input type="submit"  title=${pack2} name="pack2" value="Pack 2" />
				</form>
			</div>
			</#if>
			<#if pack3??>
			<div class="divTableCell">
				<form action="wordGame" method="POST">
					  <input type="hidden" name="storyId" value="${storyId}"/>
					  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					  <input type="submit"  title=${pack3} name="pack3" value="Pack 3" />
				</form>
			</div>
			</#if>
			<#if pack4??>
			<div class="divTableCell">
				<form action="wordGame" method="POST">
					  <input type="hidden" name="storyId" value="${storyId}"/>
					  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					  <input type="submit"  title=${pack4} name="pack4" value="Pack 4" />
				</form>
			</div>
			</#if>
			<#if pack5??>
			<div class="divTableCell">
				<form action="wordGame" method="POST">
					  <input type="hidden" name="storyId" value="${storyId}"/>
					  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					  <input type="submit"  title=${pack5} name="pack5" value="Pack 5" />
				</form>
			</div>
			</#if>
		</div><!--divTableRow-->
	</div><!--divTableBody-->
  </div><!--divTable for words-->
 </div><!--selector-container-->
 
  <hr>
  
  <div class="divTable">
		<div class="divTableRow">
			<div class="divTableCell">
				<h3 id="idioms-title"><@spring.message 'gameSelector.idioms'/></h3>
			</div>
		</div><!--divTableRow-->
  </div><!--rTable for idioms-->
</body>
</html>
