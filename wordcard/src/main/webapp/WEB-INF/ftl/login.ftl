<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="resources/css/login.css">
  <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">
   
  <title><@spring.message 'login.headerTitle'/></title>
</head>
  
<body>

	<div id="divMain">
	    <div id="divLoginTitle">
       	   <@spring.message 'login.title'/>
       	</div>
		<div id="divLoginFrame">
			<fieldset>
				<#if error??>
			    	<div class="messageDiv"><@spring.message 'login.emailOrPasswordError'/></div>
			    </#if>
			    <#if msg??>
			       <div class="messageDiv"><@spring.message 'login.loggedOut'/></div>
			    </#if>
			     
				<!-- the value of the "action" is used in @RequestMapping(value = "/login",...) -->
				<form action="login" method="POST">
					<div class="divTable">
						<div class="divTableBody">
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'login.email'/></div>
								<div class="divTableCell"><input type="text" id="email" name="email" /></div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'login.password'/></div>
								<div class="divTableCell"><input type="password" name="password" id="password" /></div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"></div>
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								<div class="divTableCell">
								  <div class="right">
								    <input type="submit" value="<@spring.message 'login.loginButton'/>" />
								  </div>
								</div>
							</div>
						</div>
					</div><!-- divTable -->
				</form>
				<a href="forgot-password"><@spring.message 'login.forgotPassword'/></a>
				<div class="right">
					<a href="registration"><@spring.message 'login.newRegistration'/></a>
				</div>
			</fieldset>
			</div><!-- divLoginFrame -->
	     </div><!-- divMain -->

</body>
</html>
