<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>
<head>
<title>Test page for JavaCript and CSS</title>

 <meta name="_csrf" content="${_csrf.token}"/>
 <!-- default header name is X-CSRF-TOKEN -->
 <meta name="_csrf_header" content="${_csrf.headerName}"/>
 
 
<script src="resources/lib/jquery-2.2.1.min.js"></script>
<script src="resources/lib/jquery.validate.js"></script>
<script src="resources/lib/crunchify.js"></script>

<!-- during page load the this function is called automatically -->
<script>
	$(function(){
	   hideTexts();
	});
</script>

</head>
<body>



<input id="activeText1"  type="text" value="Secret message"><br>
<input id="activeText2"  type="text" value="Secret message"><br>
<input id="activeText3"  type="text" value="Secret message"><br>
<input id="activeText4"  type="text" value="Secret message"><br>

<input type="button" value="Hint" onClick="toggle()" /><br>
<input type="button" value="hide texts" onClick="hideTexts()" />


<h1>Ajax Test</h1>

<form id="search-form">

 User name: <input type=text class="form-control" id="username"> mkyong<br>
 Email:     <input type="text" class="form-control" id="email"> mkyong@yahoo.com<br>
 <button type="submit" id="bth-search">Search</button>
 <br>
 <div id="feedback"></div>
 
</form>
	
<script>
	

</script>

</body>
</html>
