<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="resources/css/profile.css">
  <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">
  <title>Welcome!</title>
</head>
<body>

  <#-- Greet the user with his/her name -->
  
  <div id="main-container">
	  
	  <div id="common-container">
		 	<a href="main">To Main page</a>
	  </div>
	  
	  <div class="center">
		  <h3>Profile</h3>
	  </div>
	  <div class="divTable">
		<div class="divTableBody">
			<div class="divTableRow">
				<div class="divTableCell">Name:</div>
				<div class="divTableCell">${user.name}</div>
			</div>
			<div class="divTableRow">
				<div class="divTableCell">Email:</div>
				<div class="divTableCell">${user.email}</div>
			</div>
			<div class="divTableRow">
				<div class="divTableCell">Reg. Date:</div>
				<div class="divTableCell">${user.dateOfRegistration?string('yyyy.MM.dd HH:mm:ss')}</div>
			</div>
			<div class="divTableRow">
				<div class="divTableCell">Valid:</div>
				<div class="divTableCell">${user.validUser?c}</div>
			</div>
		</div>
	  </div><!-- divTable -->
 </div>
</body>
</html>
