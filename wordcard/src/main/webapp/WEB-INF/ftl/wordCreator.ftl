<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>

<head>
 <meta charset="utf-8">
 <link rel="stylesheet" type="text/css" href="resources/css/common.css">
 <link rel="stylesheet" type="text/css" href="resources/css/wordCreator.css">
 <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">

 <script type="text/javascript" src="resources/lib/jquery-2.2.1.min.js"></script>
 <script type="text/javascript" src="resources/lib/jquery.caret.js"></script>
 <script type="text/javascript" src="resources/lib/jquery.i18n.properties-min-1.0.9.js"></script>
 <script type="text/javascript" src="resources/lib/wordCreator.js"></script>
 
 <meta name="_csrf" content="${_csrf.token}"/>
 <meta name="_csrf_header" content="${_csrf.headerName}"/>

 <title><@spring.message 'wordCreator.headerTitle'/></title>

<script type="text/javascript">

</script>

</head>

<body>
	<div id="content">
	    <div id="common-container">
	      <a href="main"><@spring.message 'common.toMainPage'/></a>
	      <a href="help"><@spring.message 'common.help'/></a>
	      <a href="logout"><@spring.message 'common.logout'/></a>
	    </div>
	    <div class="center">
	      <h1><@spring.message 'wordCreator.title'/></h1>
	    </div>
	   
	    <form id="addWord-form">
	    <div class="divTable">
			<div class="divTableBody">
				<div class="divTableRow">
					<div class="divTableCellBottom">
					   	<h3><@spring.message 'wordCreator.story'/></h3>
					</div>
					<div class="divTableCellVerticallAligng">
					    <div>
							<select id="storyNameSelector" name="storyText" >
								<#list stories as storyName>
									<option value="${storyName}">${storyName}</option>
								</#list>
								<option value="other"><@spring.message 'wordCreator.createNewStory'/></option>
							</select>
					    </div>
						<div id="newStoryDiv">
							<input name="storyName" id="storyName" maxlength="50" required />
						</div>
					</div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><h3><@spring.message 'wordCreator.word'/></h3></div>
					<div class="divTableCell"><input type="text" id="word" name="word" required/></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><h3><@spring.message 'wordCreator.pronunciation'/></h3></div>
					<div class="divTableCell">
					  <input name="pronunciation" id="pronunciation" required maxlength="50"/>  
					    <div id="spec-chars">
							 		<label onclick="insertCharToPronun('ʒ')">ʒ</label>
                                    <label onclick="insertCharToPronun('θ')">θ</label>
                                    <label onclick="insertCharToPronun('ð')">ð</label>
							 		<label onclick="insertCharToPronun('ʃ')">ʃ</label>
							 		<label onclick="insertCharToPronun('ŋ')">ŋ</label>
							 		<label onclick="insertCharToPronun('æ')">æ</label>
							 		<label onclick="insertCharToPronun('ɒ')">ɒ</label>
							 		<label onclick="insertCharToPronun('ʌ')">ʌ</label>
							 		<label onclick="insertCharToPronun('ʊ')">ʊ</label>
							 		<label onclick="insertCharToPronun('ə')">ə</label>
							 		<label onclick="insertCharToPronun('ɔ')">ɔ</label>
							 		<label onclick="insertCharToPronun('ɜ')">ɜ</label>
							 	</div>
					  </div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><h3><@spring.message 'wordCreator.definition'/></h3></div>
					<div class="divTableCell"><textarea name="definition" id="definition" rows="5" cols="100" required maxlength="250"></textarea></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><h3><@spring.message 'wordCreator.usage'/></h3></div>
					<div class="divTableCell"><textarea name="usage" id="usage" rows="10" cols="100" required maxlength="505"></textarea></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"></div>
					<div class="divTableCell">
					  <div class="right">
					   <input id="btn-save" type="submit" value="Save" />
					    </div>
					</div>
				</div>
			</div>
		</div><!-- divTable -->
		</form>
	</div><!-- content -->
	
	<div id="feedback"></div>
	
</body>
</html>
