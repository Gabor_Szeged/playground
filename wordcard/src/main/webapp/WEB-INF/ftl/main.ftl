<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring/>

<!DOCTYPE html>
<html>
<head>

 <link rel="stylesheet" type="text/css" href="resources/css/main.css">
 
<title><@spring.message 'main.headerTitle'/></title>

</head>

<body>
	<!-- <h1 class="title-header"> This is the main page</h1>-->
	
	
	<div id="main-container">
	  
	  <div id="common-container">
		 	<a href="help"><@spring.message 'common.help'/></a>
		 	<a href="profile"><@spring.message 'common.profile'/></a>
		  	<a href="logout"><@spring.message 'common.logout'/></a>
	  </div>
	  
	  <div class="center">
	  
		<div id="game-container">
		 <h3> <@spring.message 'main.game'/> </h3>
		  <div class="content">
		    <a href="gameSelector"><@spring.message 'main.selectGame'/></a>
		  </div>
		</div>
		
		<div id="words-container">
	  	 <h3><@spring.message 'main.words'/></h3>
	  	  <div class="content">
	  	    <a href="createNewWord"><@spring.message 'main.addNewWords'/></a>
	  	   </div>
	  	   <div class="content">
	  	     <a href="gotToEditWord"><@spring.message 'main.editWords'/></a>
	  	   </div>
		</div>
		
		<div id="idioms-container">
	     <h3><@spring.message 'main.idioms'/></h3>
		   <div class="content">
		     <!--<a href="createNewIdiom"><@spring.message 'main.addNewIdioms'/></a>-->
		     <a href="underConstruction"><@spring.message 'main.addNewIdioms'/></a>
		   </div>
	       <div class="content">
	         <!--<a href="/learning/gotToEditIdiom"><@spring.message 'main.editIdioms'/></a>-->
	         <a href="underConstruction"><@spring.message 'main.editIdioms'/></a>
	       </div>
		</div> <!-- idioms-container-->
		
	 </div> <!-- center --> 
	</div><!-- main-container -->


</body>
</html>
