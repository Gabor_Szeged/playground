<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html>
<head>
 
 <link rel="stylesheet" type="text/css" href="resources/css/help.css">

 <title>WordPlaySegítség</title>
</head>

<body>

	<div id="main-container">
	  <div id="common-container">
		<a href="main">Kezdőlapra</a>
	  </div>
	  <div id="helpContent-container">
		<h1>Hogyan használjuk a Szókártyákat</h1>
		
		<p>
		 Ezt a játékot egyszerű kártyalapokkal is lehet játszani de ez az alkalmazás a kártyák kezelését sokkal egyszerűbbé teszi.
		  Nincsenek előre beállított szavak. Mindenki a saját szavaival fog játszani. A kártyák 5 pakliba lesznek csoportosítva. Az ismeretlen szavak az első pakliba az ismertek az 5. pakliba kerülnek. 
		  A lényeg, hogy minden szó átkerüljön az 5. pakliba. Ezek a paklik történetek belül vannak.
		</p>
		
		<h3>Mi a szókártya</h3>
		<p>
		Egy eszerű kis lap, aminek az egyik felén egy szó a másik felén pedig a kiejtése, definíciója és példamondatok találhatóak. A kártyák pedig különböző paklikban vannak rendezve, attól függően, hogy mennyire tudjuk vagy nem tudjuk az adott szót.
		</p>
		
			<h3>Hogyan érdemes használni a kártyákat</h3>
		<ol>
		 <li>Keress egy hosszú cikket vagy történetet aminek megvan a hanganyaga is és legyen benne legalább 200 ismeretlen szó.</li>
		 <li>Szótározz ki kb 50-80 szót.</li>
		 <li>Kezdheted a játékot. Válszd az első paklit. Először a szó definícióját látod. Ha ez alapján határozottan beugrik a szó és tudsz hozzá mondani egy példamondatos és még le is tudod írni helyesen egy papírra vagy legalább hangosan betűzni, akkor mondhatod, hogy tudod ezt a szót.
		     A súgó gomb megyomásával megmutatja a használatát. Az itt talált mondatokat addig kell gyakorolni amíg <b>kívülről nem megy</b> és csak utána érdemes a következő szót kérni. A kiejtést is érdemes nézni, főleg a hangsúlyra kell ügyelni. <b> A hanganyatot a szavak tanulása közben folyamatosan hallgatni kell.</b>
			 Amior a mondtot kívűlről monjuk, akkor igyekezzünk minél jobban képileg is elképzelni.</li>
		 <li>Ha minden szó átkerült a második pakliba, akkor olvasd át az eddig kiszótározott szöveget egyszer vagy kétszer.</li>
		 <li>Most a mádodik paklival kell ugyan ezt ejátszani. Ezt addig kell folytatni amíg minden kártya átkerült az ötödik pakliba</li>
		 <li>Lehet folyltatni a szöveg szótárazását a következő 50-80 ismeretlen szóig.</li>
		 <li><b>Extra tipp:</b> Néha azért előbukkanak random szerűen is érdekes szavak, amiket szeretnénk megtanulni. Erre érdemes mondjuk egy "Mix" nevű történetet létrehozni és ehez adni ezeket a szavakat.</li>
		</ol>
		
		<h3>Új szó felvitele</h3>
		<p>
		 <ul>
		   <li>Válszd a főlapon az "Új szó hozzáadása" linket</li>
		   <li><b>Történet:</b> Ha már meglévő történethez szeretnéd az új szót hozzáadni, akkor a kis nyílra kattinva (történet beviteli mezője)a legördülő menüből lehet kiválasztani. Ha új történethez lesz hozzáadva, akkor pedig be kell írni a történet nevét.</li>
		   <li><b>Szó:</b> A szónál renhagyó ige esetén a multidőket is be lehet írni, vagy több szót is ha például több szavas igéről van szó (phrasal verb)</li>
		   <li><b>Kiejtés:</b> A kiejtés speciális karaktereit a beviteli mező melletti jelekre kattinva lehet beírni.</li>
		   <li><b>Meghatározás:</b> A definícióhoz ha nagyon muszáj, akkor lehet magyarul is írni, de azért legyen angol meghatározás is. A szónak csak azt a definícióját érdemes megadni amit az olvasott történetben jelent. Majd ha találkozol ennek a szónak más jelentésével is, akkor az egy külön kártyára fog kerülni.</li>
		   <li><b>Használat:</b> A használatot pedig természetesen angolul és a példamontatot a történetből kell venni. Esetleg kiegíszítésként még lehet más mondatokat is hozzáadni, ha a szót ugyan abban a jelentésben használja</li>
		   <li>Mehet a mentés</li>
		 </ul>
		</p>
		
		<h3>Szavak szerkeszétse</h3>
		<p>
		 <ul>
		   <li>Válszd a főlapon az "Szavak szerkeszése" linket</li>
		   <li>A kereső mezőbe írd be a keresendő szót vagy egy részét. A * több karakter helyettesít, a . egy tetszőleges karaktert. Például, ha csak a *-ot írod be, akkor az összes szók kilistázza, a b* pedig az összes b-vel kezdődó szót.</li>
		   <li>A kilistázott szóra kattintva a táblázat automatikusan kitöltődik.</li>
		   <li>A pakli és a történet kivételével minden szerkeszthető</li>
		   <li>Mehet a mentés</li>
		 </ul>
		</p>
		
		<h3>Játék a szavakkal</h3>
		<p>
		 <ul>
		   <li>Válszd a főlapon az "Válassz játékot" linket</li>
		   <li>A "Játék választó" lapon először a történetet kell kiválasztani a legördülő menüből, majd enter.</li>
		   <li>Azokhoz a paklikhoz jelennek meg gombok, amelyek tartalmaznak kártyákat.</li>
		   <li>Válaszd a soron következő paklit. Az egyesben vannak az ismeretlen szavak az ötösben pedig a megtanult szavak.</li>
		   <li>Először a szónak csak a definíciója látható, majd a "Súgó" gomb megnyomásával a többit is megmutatja.</li>
		   <li>Ha tudtad a szót akkor a jelölő négyzetbe be kell rakni a pipát, így ez a szó a következő pakliba fog kerülni, ha nem tudtad akkor üresen kell hagyni és a legelső pakliba fog visszakerülni :)</li>
		   <li>Lehet kérni a következő szót.</li>
		 </ul>
		</p>
		
	
		
		</p>
		<h3> Példa egy szó felvitelére</h3>
		<label style="color: #5e9ca0;">"When the bird started to <span style="color: #FF0000;">warble</span> an enchanting tune, Princess Rose joined it in a song, and everyone in the kingdom fell asleep and had sweet dreams till the break of dawn."</label>
		<p>
		Találtunk egy ismeretlen szót amit most pirossal jelöltem.
		</p>
		<p>
		 <span style="text-decoration: underline;">Kiejtés:</span> wɔːbl
		</p>
		<p>
		 <span style="text-decoration: underline;">Meghatározás:</span>
		  (of a bird) to sing pleasantly, 
		  to sing, especially in a high voice,
		  kellemes hangon énekel (leginkább madár)
		  énekel, különosen magas hangon 
		</p>
		<p>
		 <span style="text-decoration: underline;">Használat:</span> When the bird started to warble an enchanting tune..
		</p>
		<b>
		Info: Ez az alkalmazás még fejleszét alatt van. A UI kinézete is kíván még egy kis módosítást.
		      Ha bármi problémád vagy ötleted van a Szókártya programmal kapcsolatban, akkor ezen a címen lehet jelezni: <font size="5" color="blue">shift48@hotmail.com</font>
		      </b> 
		      Üdvözlettel, Nagy Gábor
		</p>
		
	  </div>
	</div>


</body>
</html>
