<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html>
<head>
 
 <link rel="stylesheet" type="text/css" href="resources/css/help.css">

 <title>WordPlay Help</title>
</head>

<body>

	<div id="main-container">
	  <div id="common-container">
		<a href="main">To Main page</a>
	  </div>
	  <div id="helpContent-container">
		<h1>How to use the Word Cards</h1>
		
		<p>
		This game can be played with simple paper cards but this application makes the handling of the cards much easier.
		There are no prefilled cards. Everybody will play with their own cards. The cards are organized in 5 packs. The unknown words are in the 1st pack and the well known words are in the 5th pack.
		The point is the at the end all the cars must go into the 5th pack. These packs are inside a separate story.
.		</p>
		
		<h3>What is a Word Card</h3>
		<p>
		A simple card on which one side there is a word and on the other side you can find its pronunciation, definition and usages.
		</p>
		
			<h3>How should I use the Word Cards</h3>
		<ol>
		 <li>Take a long article or story which has its soundtrack and it contains at least 200 unknown words.</li>
		 <li>Look up about 50-80 words from the story and make cards for them.</li>
		 <li>You can start playing. Choose the 1st pack. You will see at first the definition of the word. If you can figure out the word from its definition and you can say a sentence with it and even you can spell it correctly then you can say that you know that word.
		     Pressing the "Hint" button you'll see its usage. The sentence or sentences listed here <b>must be learned by heart</b> and only after that ask for the next word. Of course it's worth checking the pronunciation especially where the stress is. <b>You must listen the soundtrack in your free time every day.</b>
			 When you are learning the sentences by heart try and imagine what you are saying.
		 </li>
		 <li>If every word was moved to the next pack then read that part of the story, from which you collected the words, again once or twice.</li>
		 <li>Now you can play the same procedure with the next pack and continue it until all the cards are moved to the 5th pack.</li>
		 <li>You can continue collecting again about 50-80 new unknown words.</li>
		 <li><b>Extra tip:</b> Sometimes you come across with random words from different areas which you would also like to learn. Just add these word to a new story named let's say "Mix"</li>
		</ol>
		
		<h3>Adding a new word</h3>
		<p>
		 <ul>
		   <li>Choose the "Add new word" on the main page.</li>
		   <li><b>Story:</b> If you wan to add the new word to a previous story then clik on the small down arrow (in the story input filed) and select the story from the list. If you wan to add it to a new story then just type the name of the story.</li>
		   <li><b>Word:</b> In case of irregular verbs you can enter its past tenses or if that word is a phrasl verb, so sometimes it is necessary to enter more than a single word.</li>
		   <li><b>Pronunciation:</b> The special characters can be found next to the input field. Just click in the appropriate one.</li>
		   <li><b>Definition:</b> For the definition if it is necessary then you can use your own language but try to explain it in English too. Use only that specific definition which is suitable to the context. When you find the same word with different meaning then you will create a new card for it.</li>
		   <li><b>Usage:</b> The usage must be in English and the sentence must be from the story. You can add extra sentences but be careful that the word is used with the same meaning.</li>
		   <li>Press Save</li>
		 </ul>
		</p>
		
		<h3>Editing Words</h3>
		<p>
		 <ul>
		   <li>Choose the "Edit Words" on the main page</li>
		   <li>In the search input field enter the whole or part of the word you want to edit. The * can replace any number of characters and the . only one character. For example a single * will find all the words you added, b* will find the those which start with letter b and so on.</li>
		   <li>Clicking on the wanted word its content will be entered into the table.</li>
		   <li>The "Pack" and "Story" cannot be changed but all the other properties are freely editable.</li>
		   <li>Press Save</li>
		 </ul>
		</p>
		
		<h3>Playing with Words</h3>
		<p>
		 <ul>
		   <li>Choose "Select Game" on the main page.</li>
		   <li>On the page "Game Selector" first select the Story and press Enter (or click next to the input field).</li>
		   <li>You will see buttons only for those packs which contains cards.</li>
		   <li>Choose the next pack. The smaller the number of the pack the less known the words in it.</li>
		   <li>At first you can see only the definition of the word then with the "Hint" button you can see the usage and the word itself.</li>
		   <li>If you know the word then put a tick in the "I know it" box so this word will be moved into the next pack. If you do not know the word then it goes back into the first pack :)</li>
		   <li>You may ask for the next word.</li>
		 </ul>
		</p>
		
		<h3> Example </h3>
		<label style="color: #5e9ca0;">"When the bird started to <span style="color: #FF0000;">warble</span> an enchanting tune, Princess Rose joined it in a song, and everyone in the kingdom fell asleep and had sweet dreams till the break of dawn."</label>
		<p>
		We found an unknown word marked with red.
		</p>
		<p>
		 <span style="text-decoration: underline;">Pronunciation:</span> wɔːbl
		</p>
		<p>
		 <span style="text-decoration: underline;">Definition:</span>
		  (of a bird) to sing pleasantly, 
		  to sing, especially in a high voice, 
		</p>
		<p>
		 <span style="text-decoration: underline;">Usage:</span> When the bird started to warble an enchanting tune..
		</p>
		
		<p>
		<b>
		Info: This application is under development and some of its functions are not implemented. Also the design of the UI can be improved.
		      If you have any problems or ideas with this Word Card just drop me a mail <font size="5" color="blue">shift48@hotmail.com</font>
		      </b>
		      Thanks, Gabor Nagy.
		</p>
				
	  </div>
	</div>


</body>
</html>
