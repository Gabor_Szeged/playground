<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="resources/css/forgotPassword.css">
  <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">
   
  <title><@spring.message 'forgotPassword.headerTitle'/></title>
</head>
  
<body>

	<div id="divMain">
	    
		<div class="centerDiv">
				<form action="password-reset" method="POST">
					<div class="divTable">
						<div class="divTableBody">
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'forgotPassword.email'/></div>
								<div class="divTableCell"><input type="text" id="email" name="email" /></div>
								 <div>
								 	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
								    <input type="submit" value="<@spring.message 'forgotPassword.SendButton'/>" />
								 </div>
							</div>
						</div>
					</div><!-- divTable -->
				</form>
					 <#if message??>
			       <div class="messageDiv">${message}</div>
			    </#if>
			</div><!-- centerDiv -->
	     </div><!-- divMain -->

</body>
</html>
