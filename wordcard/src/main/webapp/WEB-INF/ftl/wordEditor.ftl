<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html>

<head>
 <meta name="_csrf" content="${_csrf.token}"/>
 <meta name="_csrf_header" content="${_csrf.headerName}"/>

 <link rel="stylesheet" type="text/css" href="resources/css/common.css">
 <link rel="stylesheet" type="text/css" href="resources/css/wordEditor.css">
 <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">

 <script type="text/javascript" src="resources/lib/jquery-2.2.1.min.js"></script>
 <script type="text/javascript" src="resources/lib/jquery.caret.js"></script>
 <script type="text/javascript" src="resources/lib/jquery.i18n.properties-min-1.0.9.js"></script>
 <script type="text/javascript" src="resources/lib/wordEditor.js"></script>

 <title><@spring.message 'wordEditor.headerTitle'/></title>
</head>

<body>
<div id="content">
	<div id="common-container">
	    <a href="main"><@spring.message 'common.toMainPage'/></a>
	    <a href="help"><@spring.message 'common.help'/></a>
	    <a href="logout"><@spring.message 'common.logout'/></a>
	</div>
	<div class="center">
	    <h1><@spring.message 'wordEditor.title'/></h1>
	</div>
	
	<div id="search-container">
		<form id="search-form">
			<div>
				<div id="tooltip-label-container">
			    	<label id="tooltip-text">*=any characher, .=single character</label>
				</div>
				<div id="search-input-container">
					<input type="text" id="searchText" />
					<input type="submit" id="btn-search" value="Search" title="*=any characher, .=single character" />
				</div>
			</div>
		</form>
			
		<div id="list-container">
		</div>
	</div> <!-- search-container -->
	
	<form id="save-form">
	    <div class="divTable">
			<div class="divTableBody">
				<div class="divTableRow">
				    <input type="hidden" id="wordId"/>
					<div class="divTableCell"><@spring.message 'wordEditor.pack'/></div>
					<div class="divTableCell"><input type="text" id="pack" name="pack" disabled/></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><@spring.message 'wordEditor.story'/></div>
					<div class="divTableCell"><input type="text" id="story" name="story" disabled/></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><@spring.message 'wordEditor.word'/></div>
					<div class="divTableCell"><input type="text" id="word" name="word" required maxlength="40" /></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><@spring.message 'wordEditor.ronunciation'/></div>
					<div class="divTableCell">
					  <input name="pronunciation" id="pronunciation" required maxlength="50" />  
					    <div id="spec-chars">
							 		<label onclick="insertCharToPronun('ʒ')">ʒ</label>
                                    <label onclick="insertCharToPronun('θ')">θ</label>
                                    <label onclick="insertCharToPronun('ð')">ð</label>
							 		<label onclick="insertCharToPronun('ʃ')">ʃ</label>
							 		<label onclick="insertCharToPronun('ŋ')">ŋ</label>
							 		<label onclick="insertCharToPronun('æ')">æ</label>
							 		<label onclick="insertCharToPronun('ɒ')">ɒ</label>
							 		<label onclick="insertCharToPronun('ʌ')">ʌ</label>
							 		<label onclick="insertCharToPronun('ʊ')">ʊ</label>
							 		<label onclick="insertCharToPronun('ə')">ə</label>
							 		<label onclick="insertCharToPronun('ɔ')">ɔ</label>
							 		<label onclick="insertCharToPronun('ɜ')">ɜ</label>
						</div>
					 </div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><@spring.message 'wordEditor.definition'/></div>
					<div class="divTableCell"><textarea name="definition" id="definition" rows="5" cols="100" required maxlength="250" ></textarea></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"><@spring.message 'wordEditor.usage'/></div>
					<div class="divTableCell"><textarea name="usage" id="usage" rows="10" cols="100" required maxlength="505" ></textarea></div>
				</div>
				<div class="divTableRow">
					<div class="divTableCell"></div>
					<div class="divTableCell">
					  <div class="right"><input id="btn-save" type="submit" value="Save" /></div>
					</div>
				</div>
			</div>
		</div><!-- divTable -->
		</form>

	<div id="feedback"></div>
	</div> <!-- content -->
 </body>
</html>
