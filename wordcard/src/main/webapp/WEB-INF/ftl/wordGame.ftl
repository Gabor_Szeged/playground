<#import "/spring.ftl" as spring />
<html>


<head>
 <script src="resources/lib/jquery-2.2.1.min.js"></script>
 <script src="resources/lib/wordGame.js"></script>
 <link rel="stylesheet" type="text/css" href="resources/css/word_game.css">
 <link rel="stylesheet" type="text/css" href="resources/css/divTable.css">

 <script>
	$(function(){
	   hideTexts();
	});
 </script>
 
 
 <title><@spring.message 'wordGame.headerTitle'/>WordGame</title>
</head>

<!--<h1>Play with Words</h1>-->
<body>
	<div id="content">
	    <div id="common-container">
	      <a href="gameSelector"><@spring.message 'wordGame.selectNewGame'/></a>
		  <a href="main"><@spring.message 'common.toMainPage'/></a>
	      <a href="logout"><@spring.message 'common.logout'/></a>
		</div>
	
	    <div id="titleContainer">
		 <label id="titleLabel">${title}</label>
	    </div>
	    <div id="storyContainer">
	      <label id="storyLabel"><@spring.message 'wordGame.story'/> </label>
	      <label id="storyTextLabel">${storyText}</label>
	    </div>
	    
		<div id="innerContainer">
			<#if warning??>
				<font size="3" color="red">${warning}</font>
			<#else>
				<form action="nextWord" method="POST">
					<input type="hidden" name="cardId" value="${wordCard.id}"/>
					<div class="divTable">
						<div class="divTableBody">
							<div class="divTableRow">
								<div class="divTableCell"></div>
								<div class="divTableCell">
								 	<div class="right">
								 		<input id="hintButton" type="button" value="<@spring.message 'wordGame.hint'/>" name="hintButton" onClick="toggle()"/>
										<input id="hiddenStoryText" type="hidden" name="storyText" value="${storyText}"/>
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
										
										<!--
										<input type="radio" name="knownWord" id="IKI" value="1"/>
										<label for="IKI">I know it</label>
										
										<input type="radio" name="knownWord" id="IDNKI" value="2" checked/>
										<label for="IDNKI">I do not know it</label>
										
										<input type="radio" name="knownWord" id="IID" value="3"/>
										<label for="IID">It is difficult</label>
										-->
										<input id="bt-next" type="submit" value="Next" />
										<input type="checkbox" name="knownWord" value="toNextPackage"><@spring.message 'wordGame.checkBoxIknowIt'/>
									</div>
								</div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'wordGame.word'/></div>
								<div class="divTableCell"><input type="text" id="word" name="word" value="${wordCard.word}" /></div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'wordGame.pronunciation'/></div>
								<div class="divTableCell">
									<#if wordCard.pronunciation??>
										<input name="pronunciation" id="pronunciation" value="${wordCard.pronunciation}"/>
									<#else>
										<input name="pronunciation" id="pronunciation"/>
									</#if>
								</div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'wordGame.definition'/></div>
								<div class="divTableCell">
									<#if wordCard.definition??>
										<textarea name="definition" id="definition" rows="5" cols="100" required="true" maxlength="250">${wordCard.definition}</textarea>
									<#else>
										<textarea name="definition" id="definition" rows="5" cols="100" required="true" maxlength="250">???</textarea>
									</#if>
								</div>
							</div>
							<div class="divTableRow">
								<div class="divTableCell"><@spring.message 'wordGame.usage'/></div>
								<div class="divTableCell">
									<div class="right">
										<#if wordCard.usages??>
											<textarea name="usage" id="usage" rows="10" cols="100" required="true" maxlength="505">${wordCard.usages}</textarea>
										<#else>
											<textarea name="usage" id="usage" rows="10" cols="100" required="true" maxlength="505">???</textarea>
										</#if>
									</div>
								</div>
							</div>
						</div><!-- divTableBody -->
					</div><!-- divTable -->
				</form>
			</#if>
		</div> <!-- innerContainer -->
	</div> <!-- contnet -->

</body>
</html>
